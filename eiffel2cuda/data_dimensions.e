note
	description: "Dimensions of some data. Mostly used in conjunction with single- and multi- dimensional arrays"
	author: "Alexey Kolesnichenko."
	date: "$Date$"
	revision: "$Revision$"

class DATA_DIMENSIONS

create
	empty,
	make_1d,
	make_2d,
	make

feature {NONE}
	empty
	do
		dimensions := 1
		create dim_store.make (1)
		dim_store.extend(0)
	ensure
		dimensions = 1
		x_dimension = 0
	end
	make_1d(x: INTEGER)
	require
		x >= 0
	do
		dimensions := 1
		create dim_store.make (0)
		dim_store.extend (x)
	end

	make_2d(x, y: INTEGER)
	require
		x > 0
		y > 0
	do
		dimensions := 2
		create dim_store.make (0)
		dim_store.extend (x)
		dim_store.extend (y)
	end

	make(dimension_array: ARRAY[INTEGER])
	require
		not dimension_array.is_empty
		across dimension_array  as dim  all dim.item > 0  end
	do
		dimensions := dimension_array.count
		create dim_store.make_from_array (dimension_array)
	end
feature
	plus alias "+" (other: DATA_DIMENSIONS): DATA_DIMENSIONS
	require
		dimensions = other.dimensions
	local
		i: INTEGER
		list: ARRAYED_LIST[INTEGER]
	do
		create list.make (dimensions)

		from
			i := dim_store.lower
		until
			i = dim_store.upper + 1
		loop
			list.extend (dim_store[i] + other.dim_store[i])
			i := i + 1
		end

		create Result.make (list.to_array)
	ensure
		Result.dimensions = dimensions
		Result.x_dimension = x_dimension + other.x_dimension
		Result.y_dimension = y_dimension + other.y_dimension
	end

	dimensions: INTEGER
	attribute
	ensure
		Result > 0
	end

	dimension (i: INTEGER): INTEGER
		-- get i-th dimension. i is in range [0, dimensions - 1]
	require
		i >= 0 and i < dimensions
	do
		Result := dim_store[dim_store.lower + i]
	ensure
		Result >= 0
	end

	x_dimension: INTEGER
		-- first dimension - is always present
	do
		Result := dimension (0)
	ensure
		Result >= 0
	end

	y_dimension: INTEGER
	do
		if dimensions < 2 then
			Result := 0
		else
			Result := dimension (1)
		end
	ensure
		Result >= 0
		dimensions < 2 implies Result = 0
	end


	total_dimension_size: INTEGER
	local
		i: INTEGER
	do
		Result := 1
		from
			i := dim_store.lower
		until
			i = dim_store.upper + 1
		loop
			Result := Result * dim_store[i]
			i := i + 1
		end
	ensure
		Result >= 0
	end
feature {DATA_DIMENSIONS}
	dim_store: ARRAYED_LIST[INTEGER]
end

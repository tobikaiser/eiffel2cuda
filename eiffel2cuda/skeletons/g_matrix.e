note
	description: "GPGPU-accelerated matrix."
	author: "Alexey Kolesnichenko."
	date: "$Date$"
	revision: "$Revision$"

class G_MATRIX[T -> NUMERIC create default_create end]
inherit
	DEVICE_SKELETON[T]
	export {G_MATRIX}
		device_data
	end
	G_COLLECTION[T]
	redefine
		count
	end
create
	make, from_array2, from_folded_array, single_row, single_column
create {G_MATRIX}
	from_handle
feature {NONE}
	make(a_rows, a_columns: INTEGER)
	require
		a_rows > 0
		a_columns > 0
	local
		l_handle: CUDA_DATA_HANDLE[T]
	do
		create pending_kernels.make
		create cuda
		cuda.initialize_cuda

		create device_data.new_handle_default (create {DATA_DIMENSIONS}.make_2d(a_rows, a_columns))
	ensure
		rows = a_rows
		columns = a_columns
	end

	single_row (a_row: G_VECTOR[T])
	do
		create pending_kernels.make
		create cuda
		cuda.initialize_cuda
		create device_data.from_device_ptr (a_row.device_data.element_size,
			a_row.device_data.pointer, create {DATA_DIMENSIONS}.make_2d(a_row.count, 1))
	ensure
		rows = 1
		columns = a_row.count
	end

	single_column (a_column: G_VECTOR[T])
	do
		create pending_kernels.make
		create cuda
		cuda.initialize_cuda

		create device_data.from_device_ptr (a_column.device_data.element_size,
			a_column.device_data.pointer, create {DATA_DIMENSIONS}.make_2d(1, a_column.count))
	ensure
		rows = a_column.count
		columns = 1
	end

	from_array2 (array2: ARRAY2[T])
	do
		create pending_kernels.make
		create cuda
		cuda.initialize_cuda

		device_data := cuda.copy_multidimensional_array_to_gpu (array2,
			create {DATA_DIMENSIONS}.make_2d (array2.height, array2.width))
	ensure
		rows = array2.height
		columns = array2.width
	end

	from_folded_array(array: ARRAY[T]; a_rows, a_columns: INTEGER)
	require
		non_negative_rows: a_rows > 0
		non_negative_columns: a_columns > 0
		properly_folded: array.count = a_rows * a_columns
	do
		from_array2 (create {ARRAY2_EX[T]}.make_from_area (array.area, a_rows, a_columns))
	end

	from_handle( a_device_data: like device_data)
	do
		create pending_kernels.make
		create cuda
		cuda.initialize_cuda
		device_data := a_device_data
	end
feature
	rows: INTEGER
	do
		Result := device_data.x_dimension.to_integer_32
	end

	columns: INTEGER
	do
		Result := device_data.y_dimension
	end

	count: INTEGER
	-- total number of elements in the matrix.
	do
		Result := rows * columns
	end
feature
	multiply (other: G_MATRIX[T]): like Current
	require
		valid_for_multiplication: columns = other.rows
	do
		Result := Current
	ensure
		Result.rows = Current.rows
		Result.columns = other.columns
	end

	multiply_by_vector_column (other: G_VECTOR[T]): like Current
		-- multiplies `Current' by a vector-column `other'.
	require
		columns = other.count
	do
		Result := multiply (create {G_MATRIX[T]}.single_column (other))
	ensure
		Result.rows = Current.rows
		Result.columns = 1
	end

	multiply_by_vector_row (other: G_VECTOR[T]): like Current
		-- multiplies `Current' by a vector-row `other'. Can only be performed
		-- if `Current' is a single-column matrix
	require
		columns = 1
	do
		Result := multiply (create {G_MATRIX[T]}.single_row (other))
	ensure
		Result.columns = Result.rows
		Result.columns = Current.rows
		Result.rows = Current.rows
	end

	multiplied_by (number: T)
	local
		l_row_vector: G_VECTOR[T]
	do
		create l_row_vector.from_handle(plain_represantation)

		l_row_vector.multiplied_by(number)
	end
feature
	cell alias "()" (i, j: INTEGER): T
	do
		Result := plain_represantation.at (i * columns + j)
	end

	row (i: INTEGER): G_VECTOR[T]
	do
		create Result.from_handle (device_data.subset_1d (i * columns, i * columns + (rows - 1)))
	ensure
		Result.count = columns
	end

	for_all_rows(start_pos, end_pos: INTEGER)
	do

	end

	column (i: INTEGER): G_VECTOR[T]
	do
		create Result.from_handle(device_data)
	ensure
		Result.count = rows
	end

	submatrix (i, j: INTEGER): like Current
	do
		Result := Current
	end

	transpose: like Current
	do
		create Result.make (columns, rows)
		Result := Current
	ensure
		Result.rows = columns
		Result.columns = rows
	end
feature -- convertion
	to_array2: ARRAY2[T]
	do
		Result := device_data.to_array2
	end
feature {NONE}
	plain_represantation: CUDA_DATA_HANDLE[T]
	attribute
		create Result.from_device_ptr (device_data.element_size,
			device_data.pointer,
			create {DATA_DIMENSIONS}.make_1d (device_data.dimensions.total_dimension_size))
	end
end

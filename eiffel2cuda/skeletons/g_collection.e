note
	description: "Represents a generic collection. All range operations are executed on CUDA device."
	author: "Alexey Kolesnichenko."
	date: "$Date$"
	revision: "$Revision$"

class G_COLLECTION[T-> ANY create default_create end]
inherit DEVICE_SKELETON[T]
	export {G_VECTOR, G_MATRIX}
		device_data
	end


ITERABLE[T]
create
	from_array
feature {NONE}
	from_array(a_collection: ARRAY[T])
	do
		create pending_kernels.make
		create cuda
		cuda.initialize_cuda
		device_data := cuda.copy_array_to_gpu (a_collection)
	end

feature {NONE}
	-- stores the list of queued actions.
 	pending_kernels: LINKED_LIST[TUPLE[kernel: CUDA_KERNEL[ANY]; args: KERNEL_LAUNCH_PARAMS[T]]]
feature
	map(a_map: FUNCTION[ANY, TUPLE[T], ANY]): G_COLLECTION[ANY]
	do
		Result := Current
	end

	filter(a_predicate: PREDICATE[ANY, TUPLE[T]]): like Current
	do
		Result := Current
	end

--	filter (predicate_name, predicate_body: STRING): T
--	local
--		reduce_template: REDUCE_TEMPLATE
--		reduce_param: KERNEL_PARAMETER
--		reduce_action: REDUCTION_ACTION
--		params: ARRAY[KERNEL_PARAMETER]
--	do
--		reduce_param := kernel_param_from_me
--		reduce_param.is_output := true
--		reduce_param.is_scalar := true

--		params := <<kernel_param_from_me, reduce_param>>
--		params[1].name := "a"
--		params[2].name := "b"

--		create reduce_action.make_min (params)

--		reduce_param := kernel_param_from_me
--		reduce_param.is_output := true
--		reduce_param.is_scalar := true
--		create reduce_template.make (predicate_name, reduce_action,
--			<<kernel_param_from_me, reduce_param>>, default_block_dim_1d.x)

--		Result := reduce_operation (reduce_template)
--	end

	for_all(an_action: PROCEDURE[T, TUPLE[T]])
	do

	end

--	count_if (predicate_name, predicate_body: STRING): NATURAL_32
--	local
--		reduce_template: REDUCE_TEMPLATE
--		reduce_param: KERNEL_PARAMETER
--		reduce_action: REDUCTION_ACTION
--		params: ARRAY[KERNEL_PARAMETER]
--		l_return_type: STRING
--	do
--		reduce_param := kernel_param_from_me
--		reduce_param.is_output := true
--		reduce_param.is_scalar := true

--		params := <<kernel_param_from_me, reduce_param>>
--		params[1].name := "a"
--		params[2].name := "b"

--		l_return_type := predicate_body.substring (1, predicate_body.index_of (' ', 1))
--		create reduce_action.make_from_string (predicate_name, predicate_body, l_return_type)

--		reduce_param := kernel_param_from_me
--		reduce_param.is_output := true
--		reduce_param.is_scalar := true
--		create reduce_template.make (predicate_name + "_kernel", reduce_action,
--			<<kernel_param_from_me, reduce_param>>, default_block_dim_1d.x)

--		--Result := reduce_operation (reduce_template)
--	end

--	for_all (predicate_name, predicate_body: STRING): BOOLEAN
--			-- Is `predicate_body' true for all items?
--	do
--		Result := count_if (predicate_name, predicate_body) = count
--	end

	item alias "[]" (index: INTEGER): T
	require
		index_in_range: index >= 0 and index < count
	do
		launch_pending_kernels
		Result := device_data.at(index)
	end

	count: INTEGER
	do
		Result := device_data.x_dimension
	end

	to_array: ARRAY[T]
	do
		launch_pending_kernels
		Result := device_data.to_array
	end

feature -- Iterable implementation
	new_cursor: ITERATION_CURSOR[T]
	do
		launch_pending_kernels
		Result := to_array.new_cursor
	end
feature {NONE}
	launch_pending_kernels
	local
		l_kernel: CUDA_KERNEL[ANY]
		l_args: KERNEL_LAUNCH_PARAMS[T]
	do
		across pending_kernels as current_kernel
		loop
			l_kernel := current_kernel.item.kernel
			l_args := current_kernel.item.args
			--device_data := l_kernel.launch_with_arguments (l_args)
		end
	end

end

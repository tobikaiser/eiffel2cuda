note
	description: "Maps Eiffel types to C++ types."
	author: "Alexey Kolesnichenko"
	date: "2.07.2014"
	revision: "$Revision$"

class TYPE_MAPPER
create
	make
feature {NONE}
	make
	do
		create type_map.make (10)
		type_map["STRING"] := "std::string"
		type_map["REAL_32"] := "float"
		type_map["DOUBLE"] := "double"
		type_map["REAL_64"] := "double"
		type_map["INTEGER"] := "int"
		type_map["NATURAL"] := "uint"
		type_map["NATURAL_64"] := "long"
		type_map["INTEGER_32"] := "int"
		type_map["INTEGER_16"] := "short"
	end

feature
	map_type (type: STRING): STRING
	do
		if type_map.has (type) and
		then attached type_map[type] as l_type then
			Result := l_type
		else
			Result := "not found"
		end
	end
feature {NONE}
	type_map: HASH_TABLE[STRING, STRING]
end

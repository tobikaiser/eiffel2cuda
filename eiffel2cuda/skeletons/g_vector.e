note
	description: "Summary description for {G_VECTOR}."
	author: "Alexey Kolesnichenko"
	date: "$Date$"
	revision: "$Revision$"

class G_VECTOR [T -> NUMERIC create default_create end]
inherit
	DEVICE_SKELETON[T]
	export {G_VECTOR, G_MATRIX}
		device_data
	end
	G_COLLECTION[T]
	ITERABLE[T]
create
	from_array
create {G_VECTOR, G_MATRIX}
	from_handle
feature {NONE}
	from_handle(a_handle: like device_data)
	-- shares the provided handle.
	do
		create pending_kernels.make
		create cuda
		cuda.initialize_cuda
		device_data := a_handle
	ensure
		device_data.dimensions.dimensions = 1
		device_data.x_dimension = a_handle.dimensions.total_dimension_size
	end

feature -- arithmetic operations
	plus alias "+"(other: like Current): like Current
	require
		same_length: count = other.count
	do
		Result:= arithmetic_operation ("plus", "+", other)
	end

	minus alias "-" (other: like Current): like Current
	require
		same_length: count = other.count
	do
		Result:= arithmetic_operation ("minus", "-", other)
	end
feature -- in-place operations
	multiplied_by (number: T)
	local
		other: like Current
	do
		create other.from_handle (cuda.copy_single_item_to_gpu (number))
		in_place_arithmetic_operation ("multiplied_by", "*", other)
	end

	compwise_multiplication (other: like Current): like Current
	require
		other.count = count
	do
		Result := arithmetic_operation ("compwise_multiplication", "*", other)
	end

	compwise_division (other: like Current): like Current
	require
		other.count = count
		-- no single element is zero!
	do
		Result := arithmetic_operation ("compwise_division", "/", other)
	end

	divided_by (divisor: T)
	require
		non_zero_divisor: divisor /= divisor.zero
	local
		other: like Current
	do
		create other.from_handle (cuda.copy_single_item_to_gpu (divisor))
		in_place_arithmetic_operation ("divided_by", "/", other)
	end

	in_place_plus (other: like Current)
	require
		same_length: count = other.count
	do
		in_place_arithmetic_operation ("plus", "+", other)
	end

	in_place_minus (other: like Current)
	require
		same_length: count = other.count
	do
		in_place_arithmetic_operation ("minus", "-", other)
	end

feature -- collection-like operations

	sum: T
	local
		reduce_template: REDUCE_TEMPLATE
		reduce_param: KERNEL_PARAMETER
		reduce_action: REDUCTION_ACTION
		params: ARRAY[KERNEL_PARAMETER]
	do
		reduce_param := kernel_param_from_me
		reduce_param.is_output := true
		reduce_param.is_scalar := true

		params := <<kernel_param_from_me, reduce_param>>
		params[1].name := "a"
		params[2].name := "b"

		create reduce_action.make_sum (params)

		reduce_param := kernel_param_from_me
		reduce_param.is_output := true
		reduce_param.is_scalar := true
		create reduce_template.make ("sum_kernel", reduce_action,
			<<kernel_param_from_me, reduce_param>>, default_block_dim_1d.x)


		Result := reduce_operation (reduce_template)
	end

	max: T
	local
		reduce_template: REDUCE_TEMPLATE
		reduce_param: KERNEL_PARAMETER
		reduce_action: REDUCTION_ACTION
		params: ARRAY[KERNEL_PARAMETER]
	do
		reduce_param := kernel_param_from_me
		reduce_param.is_output := true
		reduce_param.is_scalar := true

		params := <<kernel_param_from_me, reduce_param>>
		params[1].name := "a"
		params[2].name := "b"

		create reduce_action.make_max (params)

		reduce_param := kernel_param_from_me
		reduce_param.is_output := true
		reduce_param.is_scalar := true
		create reduce_template.make ("max_kernel", reduce_action,
			<<kernel_param_from_me, reduce_param>>, default_block_dim_1d.x)


		Result := reduce_operation (reduce_template)
	end

	min: T
	local
		reduce_template: REDUCE_TEMPLATE
		reduce_param: KERNEL_PARAMETER
		reduce_action: REDUCTION_ACTION
		params: ARRAY[KERNEL_PARAMETER]
	do
		reduce_param := kernel_param_from_me
		reduce_param.is_output := true
		reduce_param.is_scalar := true

		params := <<kernel_param_from_me, reduce_param>>
		params[1].name := "a"
		params[2].name := "b"

		create reduce_action.make_min (params)

		reduce_param := kernel_param_from_me
		reduce_param.is_output := true
		reduce_param.is_scalar := true
		create reduce_template.make ("min_kernel", reduce_action,
			<<kernel_param_from_me, reduce_param>>, default_block_dim_1d.x)

		Result := reduce_operation (reduce_template)
	end
feature -- partitioning
	subvector (start_pos, end_pos: INTEGER): like Current
	require
		valid_start_pos: start_pos >= 0
		non_empty_subvector: end_pos - start_pos + 1 > 0
		valid_end_pos: end_pos < count
	do
		create Result.from_handle (device_data.subset_1d (start_pos, end_pos))
	ensure
		Result.count = end_pos - start_pos + 1
	end

	subvector_of_length (start_pos, length: INTEGER): like Current
	require
		non_negative_length: length > 0
		valid_start_pos: start_pos >= 0
		valid_end_pos: start_pos + length <= count
	do
		Result := subvector (start_pos, start_pos + length - 1)
	end
feature {NONE}
	loader: KERNEL_LOADER
	ar_util: ARRAY_UTIL

feature {NONE}
	reduce_operation (reduce_template: REDUCE_TEMPLATE): T
	local
		reduce_result: like device_data
		kernel: CUDA_KERNEL[T]
		reduce_input: like device_data
		result_data_dimensions: DATA_DIMENSIONS
		launch_params: KERNEL_LAUNCH_PARAMS[T]
	do
		create result_data_dimensions.make_1d (default_grid_dim_1d (count.to_natural_32).x.to_integer_32 )
		create reduce_result.new_handle_default (result_data_dimensions)

		reduce_input := device_data
		create kernel.make (<<reduce_input, reduce_result>>, reduce_result)
		kernel.load_kernel (
			loader.kernel_to_file (reduce_template,
					"cu_vector_math_"  + reduce_template.kernel_name + "_of_" + ({T}).name),
					reduce_template.kernel_name)
		from
		until
			reduce_input.x_dimension = 1
		loop
			create launch_params.make_with_shared_memory ( create {KERNEL_ARGS[T]}.make (<<reduce_input, reduce_result>>, reduce_result),
				default_grid_dim_1d (reduce_input.x_dimension.to_natural_32), default_block_dim_1d,
					default_block_dim_1d.x * reduce_input.element_size)
			reduce_input := kernel.launch_with_arguments (launch_params)

			create result_data_dimensions.make_1d (default_grid_dim_1d (reduce_input.x_dimension.to_natural_32).x.to_integer_32)
			create reduce_result.new_handle_default (result_data_dimensions)
		end

		Result := reduce_input.to_array[1]
	end



	in_place_arithmetic_operation (kernel_name, op_code: STRING; other: like Current)
	local
		vector_template: VECTOR_TEMPLATE
		output_param: KERNEL_PARAMETER
		kernel: CUDA_KERNEL[T]
	do
		output_param := kernel_param_from_me
		output_param.is_output := true

		create vector_template.make (kernel_name, op_code, <<output_param, other.kernel_param_from_me>>)

		create kernel.make (<<device_data, other.device_data>>, device_data)
		kernel.load_kernel (loader.kernel_to_file (vector_template,
			"cu_vector_inplace_arithmetic_" + kernel_name + "_of_" + ({T}).name),
			kernel_name)
		kernel.launch (default_grid_dim_1d (device_data.x_dimension.to_natural_32), default_block_dim_1d)
	end

	arithmetic_operation (kernel_name, op_code: STRING; other: like Current): like Current
	local
		vector_template: VECTOR_TEMPLATE
		output_param: KERNEL_PARAMETER
		kernel: CUDA_KERNEL[T]
		result_data: like device_data
	do
		output_param := kernel_param_from_me
		output_param.is_output := true

		create vector_template.make (kernel_name, op_code,
			<<kernel_param_from_me, other.kernel_param_from_me, output_param>>)

		create result_data.new_handle_default (device_data.dimensions)
		create kernel.make (<<device_data, other.device_data, result_data>>, result_data)
		kernel.load_kernel (loader.kernel_to_file (vector_template,
			"cu_vector_arithmetic_" + kernel_name + "_of_" + ({T}).name),
			kernel_name)
		kernel.launch (default_grid_dim_1d (device_data.x_dimension.to_natural_32), default_block_dim_1d)
		create Result.from_handle (kernel.output)
	end
feature {G_VECTOR, G_MATRIX}
	kernel_param_from_me: KERNEL_PARAMETER
	local
		mapper: TYPE_MAPPER
	do
		create mapper.make
		create Result.make (mapper.map_type (({T}).name), 1)
		if count = 1 then
			Result.is_scalar := true
		end
	end

	var_from_template_param (a_name: STRING): G_VAR
	local
		mapper: TYPE_MAPPER
	do
		create mapper.make
		create Result.make (a_name, mapper.map_type (({T}).name))
	end
end

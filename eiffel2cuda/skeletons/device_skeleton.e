note
	description: "A data structure skeleton, that resides on device. This base class can be used to run manually-written kernels on the incapsulated data"
	author: "Alexey Kolesnichenko."
	date: "11.07.2014"
	revision: "$Revision$"

deferred class DEVICE_SKELETON[T -> ANY create default_create end]
inherit
	GRID_FACTORY
	export {DEVICE_SKELETON}
		all
	end
feature
	launch_kernel
	do
	end

feature {NONE}
	device_data: CUDA_DATA_HANDLE[T]
end

note
	description: "Abstract class that represents a kernel on device."
	author: "Alexey Kolesnichenko"
	date: "3.07.2014"
	revision: "$Revision$"

deferred class KERNEL_TEMPLATE
feature
	kernel_name: STRING
	kernel_prototype: ARRAY[KERNEL_PARAMETER]
	return_type: STRING

	generate_kernel: STRING
	deferred
	end

feature {NONE}
	sync_threads: STRING
	do
		Result := "__syncthreads();"
	end



	tab: STRING
	do
		Result := "	"
	end
end

note
	description: "Summary description for {REDUCE_TEMPLATE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class REDUCE_TEMPLATE
inherit KERNEL_TEMPLATE
create
	make
feature
	make (a_name: STRING; a_reduction_action: REDUCTION_ACTION; parameters: ARRAY[KERNEL_PARAMETER]; shared_memory_size: NATURAL_32)
	require
		parameters.count = 2
		parameters[2].is_output
		parameters[1].type.is_equal (parameters[2].type)
	local
		inputs_list: ARRAYED_LIST[like kernel_prototype.item]
	do
		return_type := "void"
		base_str := "a"
		char_index := 1
		create idx.make_1d_global ("i")
		create thread_idx.make_1d_local ("t")
		create shared_mem_decl.make_dynamic (parameters[1].type, "shared_s", shared_memory_size)
		reduction_action := a_reduction_action
		kernel_name := a_name

		--create inputs_list.make (parameters.count)

		output := parameters[parameters.upper]
--		across parameters as param
--		loop
--			param.item.name := name
--			inputs_list.extend (param.item)

--			if param.item.is_output then
--				output := param.item
--			end
--		end

		kernel_prototype := parameters --inputs_list.to_array
	end
feature {NONE} -- name generation
	base_str: STRING
	char_index: INTEGER
	set_names
	do
		across kernel_prototype as param
		loop
			param.item.name := name

			if param.item.is_output then
				output := param.item
			end
		end
	end
	name: STRING
	do
		Result:= "vec_" + base_str
		if (base_str[char_index].code + 1 < code_of_z) then
			base_str := base_str[char_index].next.out
		else
			base_str[char_index] := 'a'
			base_str := base_str + "a"
			char_index := char_index + 1
		end
	end
feature -- codegen
	reduction_action: REDUCTION_ACTION
	shared_mem_decl: SHARED_MEMORY_DECL
	output: KERNEL_PARAMETER
	dim: INTEGER
	idx: INDEX_DECL
	thread_idx: INDEX_DECL

	fill_shared_mem: STRING
	local
		shared_mem_param: KERNEL_PARAMETER
		neutral_element: STRING
	do
		create shared_mem_param.make (kernel_prototype[1].type, 1)
		shared_mem_param.name := shared_mem_decl.name
		Result := "if (" + idx.name + " < n_" + kernel_prototype[1].name + " ) {%N"
		Result := Result + tab + thread_idx.index(shared_mem_param) + " = " + idx.index(kernel_prototype[1]) + ";%N"

		-- handling elements outside of the last block.
		-- if `reduction_action' haven't provided a neutral element
		-- the last element of the input array will be used
		neutral_element := reduction_action.neutral_element
		if neutral_element.is_empty then
			neutral_element := last_available_element_in_block
		end

		Result := Result + "} else {%N" +
			tab + thread_idx.index(shared_mem_param) + " = " + neutral_element + ";%N"
			+ "}%N"
	end

	last_available_element_in_block: STRING
	do
		Result := kernel_prototype[1].name + "[n_" + kernel_prototype[1].name + " - 1]"
	end

	generate_prototype: STRING
	do
		Result := "extern %"C%"%N__global__ "
		Result := Result + return_type + " " + kernel_name + "("

		across kernel_prototype as input
		loop
			-- T* a,
			Result := Result + input.item.type + "* " + input.item.name + ", "
			-- int n_a
			Result := Result + "int n_" + input.item.name + ", "
		end

		Result.remove_tail (2) -- removing the last occurence of ", "

		Result := Result + ") {%N"
	end

	generate_action: STRING
	local
		loop_idx: INDEX_DECL
	do
		create loop_idx.make_1d_local ("s")

		Result := "for (unsigned int " + loop_idx.name + " = blockDim.x / 2; " + loop_idx.name + " > 0;" +
			loop_idx.name + " >>= 1) { %N"
		Result := Result + "	if (" + thread_idx.name + "< " + loop_idx.name + ") {%N"
		Result := Result + "		" + shared_mem_decl.name + "["+ thread_idx.name  + "] = "
		Result := Result + reduction_action.name + "(" + shared_mem_decl.name + "["+ thread_idx.name  + "], " +
			shared_mem_decl.name + "["+ thread_idx.name + " + " + loop_idx.name + "]" + ");%N}"
		Result := Result + "%N" + tab + sync_threads + "%N"
		Result := Result + "%N}%N"

		Result := Result + tab + "if (" + thread_idx.name + " == 0) {%N"
		Result := Result + tab + tab + output.name + "[blockIdx.x] = " +  shared_mem_decl.name + "[0];%N}%N"
	end

	generate_kernel: STRING
	do
		set_names
		Result := reduction_action.out + "%N%N"
		Result := Result + generate_prototype
		Result := Result + tab + shared_mem_decl.out + "%N"
		Result := Result + tab + thread_idx.out + "%N"
		Result := Result + tab + idx.out + "%N"
		Result := Result + tab + fill_shared_mem + "%N"
		Result := Result + tab + sync_threads + "%N"
		Result := Result + generate_action
		Result := Result + "}"
	end

feature {NONE}
	code_of_z: INTEGER
	local
		z_char: CHARACTER
	do
		z_char := 'z'
		Result := z_char.code
	end
end

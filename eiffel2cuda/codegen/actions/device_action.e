note
	description: "Abstract representation of action on device."
	author: "Alexey Kolesnichenko"
	date: "6.07.2014"
	revision: "$Revision$"

deferred class DEVICE_ACTION
inherit ANY
	undefine out end
feature
	name: STRING
	arity: INTEGER
	arguments: ARRAY[KERNEL_PARAMETER]
	return_type: STRING
end

note
	description: "Summary description for {ARITHMETIC_ACTION}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class ARITHMETIC_ACTION
inherit DEVICE_ACTION
create
	make_binary
feature {NONE}

	make_binary (a_name, an_op_symbol: STRING; an_index_decl: INDEX_DECL;  arg1, arg2: KERNEL_PARAMETER)
	require
		not a_name.is_empty
		not an_op_symbol.is_boolean
		compatible_types: arg1.type.is_equal (arg2.type)
	do
		name := a_name
		arguments :=  <<arg1, arg2>>
		op_symbol := an_op_symbol
		index_decl := an_index_decl
		return_type := arg1.type
	end

feature
	out: STRING
	do
		Result := param_out (arguments[1]) + " " + op_symbol + " " + param_out (arguments[2])
	end
feature {NONE}
	op_symbol: STRING
	index_decl: INDEX_DECL
	param_out (param: KERNEL_PARAMETER): STRING
	do
		Result := param.name

		if param.dimensions > 0 then
			if param.is_scalar then
				Result := Result + "[0]"
			else
				Result := index_decl.index (param)
			end
		end
	end
end

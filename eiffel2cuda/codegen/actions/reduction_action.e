note
	description: "Represents a device function, that performs reduction-style computations (like min, max, cumilative sum, etc)"
	author: "Alexey Kolesnichenko"
	date: "8.07.2014"
	revision: "$Revision$"

class REDUCTION_ACTION
inherit	DEVICE_ACTION

create
	make_max, make_min, make_sum
feature {NONE}
	make_max (an_arguments: like arguments)
	local
		first_arg, second_arg: STRING
	do
		name := "g_max"
		arguments := an_arguments
		first_arg := arguments[1].name
		second_arg := arguments[2].name

		return_type := arguments[1].type
		action := conditional_action ( first_arg  + " > " + second_arg ,
		result_variable_name + " = " + first_arg ,
		result_variable_name + " = " + second_arg)
		neutral_element := ""
	end

	make_min (an_arguments: like arguments)
	local
		first_arg, second_arg: STRING
	do
		name := "g_min"
		arguments := an_arguments
		first_arg := arguments[1].name
		second_arg := arguments[2].name

		return_type := arguments[1].type
		action := conditional_action ( first_arg  + " < " + second_arg ,
		result_variable_name + " = " + first_arg ,
		result_variable_name + " = " + second_arg)
		neutral_element := ""
	end

	make_sum (an_arguments: like arguments)
	local
		first_arg, second_arg: STRING
	do
		name := "g_sum"
		arguments := an_arguments
		first_arg := arguments[1].name
		second_arg := arguments[2].name

		return_type := arguments[1].type
		action := tab + result_variable_name + " = " + first_arg + " + " + second_arg + ";"
		neutral_element := "0"
	end

feature

	reduction_action_prototype: STRING
	do
		-- cuda kernels are inlined by default, so no inline in prototype
		-- found at `http://stackoverflow.com/questions/5712369'
		Result := "__device__ " + return_type + " " + name + "("

		across arguments as arg
		loop
			Result := Result + arg.item.type + " " + arg.item.name + ", "
		end

		Result.remove_tail (2)
		Result := Result + ");%N"
	end


	reduction_function: STRING
	do
		-- cuda kernels are inlined by default, so no inline in prototype
		-- found at `http://stackoverflow.com/questions/5712369'
		Result := "__device__ " + return_type + " " + name + "("

		across arguments as arg
		loop
			Result := Result + arg.item.type + " " + arg.item.name + ", "
		end

		Result.remove_tail (2)
		Result := Result + ") {%N"

		Result := Result + tab + result_decl + "%N"

	end

	neutral_element: STRING
	 -- textual representation of neutral element:
	 -- i.e. for sum it's 0, for max/min - one of the elements, etc.
	 -- Can be empty - in this case, it is not possible to set it at action side
	 -- and it should be replaced at client-side (min/max case)

	out: STRING
	do
		Result := reduction_function + "%N" + action
		Result := Result + tab + "return " + result_variable_name + ";%N}"
	end

feature {NONE}
	action: STRING



	tab: STRING
	do
		Result := "	"
	end
	result_variable_name : STRING = "ret"

	result_decl: STRING
	do
		Result := return_type + " " + result_variable_name + ";"
	end

	conditional_action(condition, true_branch, false_branch: STRING): STRING
	do
		Result := "if (" + condition + ") {%N"
		Result := Result + tab + true_branch + ";%N"
		Result := Result + tab + "} else {%N"
		Result := Result + tab + false_branch + ";%N}"
	end
end

note
	description: "Summary description for {SHARED_MEMORY_DECL}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class SHARED_MEMORY_DECL
inherit ANY
	redefine out end

create
	make_static, make_dynamic
feature {NONE}
	make_static(a_type, a_name: STRING; a_requested_size: NATURAL_32)
	do
		type := a_type
		name := a_name
		requested_size := a_requested_size
		static := true
	end

	make_dynamic(a_type, a_name : STRING; a_requested_size: NATURAL_32)
	do
		type := a_type
		name := a_name
		requested_size := a_requested_size
		static := false
	end
feature
	name: STRING
	type: STRING
	requested_size: NATURAL_32
	static: BOOLEAN
	out: STRING
	do
		Result := "extern __shared__ " + type + " " + name
		if static then
			Result := Result + "[" + requested_size.out + "]"
		else
			Result := Result + "[]"
		end
		Result := Result + ";"
	end
end

note
	description: "Summary description for {INDEX_DECL}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class INDEX_DECL
inherit ANY
	redefine out end
create
	make_1d_local, make_1d_global
feature {NONE}
	make_1d_local (a_name: STRING)
	do
		name := a_name
		type := "unsigned int"
		is_local := true
	end

	make_1d_global (a_name: STRING)
	do
		name := a_name
		type := "unsigned int"
		is_local := false
	end
feature
	name: STRING
	type: STRING
	is_local: BOOLEAN

	out: STRING
	do
		Result := type + " " + name + " = "
		if not is_local then
			Result := Result + "blockIdx.x * blockDim.x + "
		end

		Result := Result + "threadIdx.x;"
	end

	index (param: KERNEL_PARAMETER): STRING
		--generates index acces for a kernel parameter, i.e. a[i]
	require
		non_scalar_param: not param.is_scalar
	do
		Result := param.name + "[" + name + "]"
	end
end

note
	description: "Represents a kernel parameter for code generation."
	author: "Alexey Kolesnichenko."
	date: "3.07.2014"
	revision: "$Revision$"

class KERNEL_PARAMETER
create
	make, make_output, make_scalar
feature {NONE}
	make(a_type: STRING; a_dimensions: INTEGER)
	require
		type_is_not_empty: not a_type.is_empty
		non_negative_dimensions: a_dimensions >= 0
	do
		type := a_type
		dimensions := a_dimensions
		name := ""
	end

	make_scalar (a_type: STRING; a_dimensions: INTEGER)
	require
		type_is_not_empty: not a_type.is_empty
		non_negative_dimensions: a_dimensions >= 0
	do
		make (a_type, a_dimensions)
		is_scalar := true
	end

	make_output(a_type: STRING; a_dimensions: INTEGER)
	require
		type_is_not_empty: not a_type.is_empty
		non_negative_dimensions: a_dimensions >= 0
	do
		make (a_type, a_dimensions)
		is_output := true
	end
feature
	type: STRING
	dimensions: INTEGER

	is_scalar: BOOLEAN assign set_scalar
		-- is a scalar (i.e. single dimension, single item)?
	attribute
	ensure
		Result implies dimensions = 1
	end
	
	is_output: BOOLEAN assign set_output

	set_output(output: BOOLEAN)
	do
		is_output := output
	end

	name: STRING assign set_name
	-- initially is not set

	is_name_set: BOOLEAN
	do
		Result := name.is_empty
	end

	set_name(a_name: STRING)
	require
		non_empty_name:	not a_name.is_empty
	do
		name := a_name
	end



	set_scalar(scalar: BOOLEAN)
	do
		is_scalar := scalar
	end
end

note
	description: "Summary description for {VECTOR_TEMPLATE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class VECTOR_TEMPLATE
inherit KERNEL_TEMPLATE
create
	make
feature
	make (a_name, op_code: STRING; parameters: ARRAY[KERNEL_PARAMETER])
	local
		inputs_list: ARRAYED_LIST[like kernel_prototype.item]
	do
		return_type := "void"
		base_str := "a"
		char_index := 1
		create idx.make_1d_global ("i")
		create action.make_binary (a_name, op_code, idx, parameters[1], parameters[2])
		kernel_name := action.name

		create inputs_list.make (parameters.count)

		output := parameters[parameters.upper]
		across parameters as param
		loop
			param.item.name := name
			inputs_list.extend (param.item)

			if param.item.is_output then
				output := param.item
			end
		end

		kernel_prototype := inputs_list.to_array
	end
feature -- name generation
	base_str: STRING
	char_index: INTEGER

	name: STRING
	do
		Result:= "vec_" + base_str

		if (base_str[char_index].code + 1 < code_of_z) then
			base_str := base_str[char_index].next.out
		else
			base_str[char_index] := 'a'
			base_str := base_str + "a"
			char_index := char_index + 1
		end
	end
feature -- codegen
	template: STRING = "foo"
	output: KERNEL_PARAMETER
	dim: INTEGER
	idx: INDEX_DECL
	generate_prototype: STRING
	do
		Result := "extern %"C%"%N__global__ "
		Result := Result + return_type + " " + kernel_name + "("

		across kernel_prototype as input
		loop
			-- T* a,
			Result := Result + input.item.type + "* " + input.item.name + ", "
			-- int n_a
			Result := Result + "int n_" + input.item.name + ", "
		end

		Result.remove_tail (2) -- removing the last occurence of ", "

		Result := Result + ") {%N"
	end

	generate_idx: STRING
	do
		Result :=  "	" + idx.out +"%N"
	end
	generate_action: STRING
	do
		Result := "	if (" + idx.name + "< n_" + kernel_prototype[1].name + ") {%N"
		Result := Result + "		" + output.name + "["+ idx.name  + "] = "
		Result := Result + action.out + ";%N"
		Result := Result + "}%N"
	end

	generate_kernel: STRING
	do
		Result := generate_prototype
		Result := Result + generate_idx
		Result := Result + generate_action
		Result := Result + "}"
	end

feature {NONE}
	action: ARITHMETIC_ACTION
	code_of_z: INTEGER
	local
		z_char: CHARACTER
	do
		z_char := 'z'
		Result := z_char.code
	end
end

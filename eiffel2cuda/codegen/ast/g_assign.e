note
	description: "Summary description for {G_ASSIGN}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class G_ASSIGN
inherit
	G_EXPR
create
	make
feature {NONE}
	make(a_lhs, a_rhs: G_EXPR)
	require
		a_lhs.type = a_rhs.type
	do
		type := a_lhs.type

		rhs := a_rhs
		lhs := a_lhs
	end

feature
	lhs: G_EXPR
	rhs: G_EXPR

	out: STRING
	do
		Result := lhs.out + " = " + rhs.out
	end
end

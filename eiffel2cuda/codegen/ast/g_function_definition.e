note
	description: "Summary description for {G_FUNCTION_DEFINITION}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class G_FUNCTION_DEFINITION
inherit
	G_AST
feature
	out: STRING
	do
		Result := "function definition"
	end
end

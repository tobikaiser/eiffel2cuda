note
	description: "Summary description for {G_VAR}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class G_VAR
inherit
	G_EXPR
create
	make
feature {NONE}
	make (a_name, a_type: STRING)
	do
		name := a_name
		type := a_type
	end
feature
	name: STRING

	out: STRING
	do
		Result := name
	end
end

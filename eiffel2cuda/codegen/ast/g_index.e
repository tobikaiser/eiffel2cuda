note
	description: "Summary description for {G_INDEX}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class G_INDEX
inherit
	G_EXPR
create
	make
feature {NONE}
	make(an_array_var: G_VAR; an_idx_expr: G_EXPR)
	do
		array_var := an_array_var
		idx_expr := an_idx_expr
		type := array_var.type
	end
feature
	array_var: G_VAR
	idx_expr: G_EXPR

	out: STRING
	do
		Result := array_var.name + "[" + idx_expr.out + "]"
	end
end

note
	description: "Summary description for {G_VAR_DECL}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class G_VAR_DECL
inherit
	G_AST
create
	make
feature {NONE}
	make (a_name, a_type: STRING)
	do
		name := a_name
		type := a_type
	end
feature
	name: STRING
	type: STRING

	out: STRING
	do
		Result := name
	end
end

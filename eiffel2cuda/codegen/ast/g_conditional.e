note
	description: "Summary description for {G_CONDITIONAL}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class G_CONDITIONAL
inherit G_AST
create
	make
feature {NONE}
	make (a_condition: G_EXPR; a_true_branch, a_false_branch: G_AST)
	require
		a_condition.type = "bool"
	do

	end
feature
	--condition: G_EXPR
	--true_branch: G_AST
	--false_branch: G_AST

	out: STRING
	do
		Result := "if (" --+ condition.out + ") { %N"
		--Result := Result + "	" + true_branch.out + "%N"
	end
end

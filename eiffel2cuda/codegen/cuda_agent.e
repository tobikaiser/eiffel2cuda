note
	description: "This class represents the current way of providing agent-style interface to cuda kernels."
	author: "Alexey Kolesnichenko"
	date: "29.08.2014"
	revision: "$Revision$"

class CUDA_AGENT
create
	make_with_name_and_body, make_from_string
feature {NONE}
	make_with_name_and_body(a_name, a_body: STRING)
	-- create an agent with it's name and body (must be written in C)
	do
		name := a_name
		return_type := extract_return_type_from_source (a_body)
		body := a_body
		if not body.starts_with(device_decl) then
			body := device_decl + body
		end

	end

	make_from_string (a_source: STRING)
	do
		body := a_source
		name := extract_name_from_source (a_source)

		return_type := extract_return_type_from_source (a_source)
	end

feature
	name: STRING -- an agent's name
	return_type: STRING -- return type
	body: STRING
feature {NONE}
	device_decl: STRING = "__device__"

	extract_return_type_from_source(a_source: STRING): STRING
	local
		start_index: INTEGER
	do
		start_index := 1
		a_source.adjust
		if a_source.starts_with (device_decl) then
			start_index := start_index + device_decl.count
		end
		Result := a_source.substring (start_index, a_source.index_of (' ', start_index))
	end

	extract_name_from_source(a_source: STRING): STRING
	local
		start_index: INTEGER
	do
		start_index := 1
		a_source.adjust
		if a_source.starts_with (device_decl) then
			start_index := start_index + device_decl.count
		end
		Result := a_source.substring (start_index, a_source.index_of (' ', start_index))
		-- Todo: finish this method
	end
end

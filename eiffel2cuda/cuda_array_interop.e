note
	description: "Provides means to copy arrayed data to device."
	author: "Alexey Kolesnichenko."
	date: "$Date$"
	revision: "$Revision$"

class CUDA_ARRAY_INTEROP[T -> ANY create default_create end]

feature
	initialize_cuda
		-- performs CUDA initialization. Will throw an exception if there are no CUDA device in the system.
	require
		cuda_available: is_cuda_available
	once
		c_initialize_cuda
	end

	copy_single_item_to_gpu (item: T): CUDA_DATA_HANDLE[T]
	do
		Result := copy_array_to_gpu (<<item>>)
	end

	concatenate (first, second: CUDA_DATA_HANDLE[T]): CUDA_DATA_HANDLE[T]
	local
		cuda_h: CUDA_HELPER
		cuda_ptr: POINTER
		new_dim: DATA_DIMENSIONS
	do
		new_dim := first.dimensions + second.dimensions
		cuda_ptr := cuda_h.cuda_allocate_memory
			(create {MEMORY_UNIT}.from_bytes (first.element_size * new_dim.total_dimension_size.to_natural_32))


		c_device_copy(first.pointer, cuda_ptr, first.total_size)
		c_device_copy(second.pointer, cuda_ptr + first.total_size.to_integer_32, second.total_size)
		create Result.from_device_ptr (first.element_size, cuda_ptr , first.dimensions + second.dimensions)
	end

	copy_multidimensional_array_to_gpu (array: ARRAY[T]; dims: DATA_DIMENSIONS): CUDA_DATA_HANDLE[T]
	require
		not_empty: array.count >= 1
		consistent_dimensions: dims.total_dimension_size = array.count
	local
		sizes: CUSTOM_SIZE[T]
		element_size: NATURAL_32
		cuda_ptr: POINTER
		cuda_h: CUDA_HELPER
	do
		element_size := sizes.size_of_default
		cuda_ptr:= c_cuda_memcpy (array.area.base_address, element_size * array.count.to_natural_32)
		create Result.from_device_ptr (element_size, cuda_ptr, dims)
	end

	copy_array_to_gpu (array: ARRAY[T]): CUDA_DATA_HANDLE[T]
	require
		not_empty: array.count >= 1
	do
		Result := copy_multidimensional_array_to_gpu(array, create {DATA_DIMENSIONS}.make_1d (array.count))
	ensure
		Result.dimensions.dimensions = 1
	end

	number_of_cuda_devices: INTEGER
	external
		"C++ inline use %"cuda.h%", %"cuda_runtime.h%", %"cuda_device_runtime_api.h%""
	alias
		"[
			int count = 0;
			cudaGetDeviceCount(&count);

			return count;
		]"
    end

    is_cuda_available: BOOLEAN
    do
    	Result:= number_of_cuda_devices > 0
    end

feature{NONE}
	c_initialize_cuda
		-- First call to cudaMalloc or cudaFree initializes the device. Found at
		-- http://stackoverflow.com/questions/15166799/any-particular-function-to-initialize-gpu-other-than-the-first-cudamalloc-call
	external
		"C++ inline use %"cuda.h%", %"cuda_runtime.h%", %"cuda_device_runtime_api.h%""
	alias
		"[
			cudaFree(0);
		]"
    end

    c_device_copy(source, target: POINTER; total_size: NATURAL_64)
    	-- on device, copies `total_size' bytes from `source' to `target'
    	-- target should point to a valid (allocated memory location.
	external
		"C++ inline use <iostream>, %"cuda.h%", %"cuda_runtime.h%", %"cuda_device_runtime_api.h%""
	alias
		"[
			cudaError_t error = cudaMemcpy($target, $source, $total_size, cudaMemcpyDeviceToDevice);
			if (error) {
				std::cerr<<"cannot copy device memory from " << $source << " to " << $target << std::endl;
				exit(-1);
			}
		]"
	end

	c_cuda_memcpy(host_ptr: POINTER; total_size: NATURAL_64): POINTER
		-- performs untyped memory copy from host to device.
		-- `host_ptr' pointer to the start of memory region (typically, address of the first element of an array)
		-- `total_size' size in bytes of all elements to be copied ( typically, calculated as array_size * element_size
		-- returns pointer to the start of memory region in device memory (do not use directly!)
	external
		"C++ inline use <iostream>, %"cuda.h%", %"cuda_runtime.h%", %"cuda_device_runtime_api.h%""
	alias
		"[
			void* gpuArray;
			cudaError_t error = cudaMalloc((void **) &gpuArray, $total_size);
			if (error) {
				std::cerr<<"cannot allocate CUDA memory"<<std::endl;
				exit(-1);
			}

			cudaMemcpy(gpuArray, $host_ptr, $total_size, cudaMemcpyHostToDevice);

			return gpuArray;
		]"
	end
end

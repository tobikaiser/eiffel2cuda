note
	description: "Helper class to get a physical size of any object."
	author: "Alexey Kolesnichenko."
	date: "20.06.2014"
	revision: "$Revision$"

expanded class
	CUSTOM_SIZE[ELEMENT -> ANY create default_create end]
feature
	size_of(e: ELEMENT): NATURAL_32
		-- gets size in bytes of any object.
	local
		special: SPECIAL[ELEMENT]
	do
		-- have to perform this hack, because `SPECIAL' does not export
		-- element size directly, so have to calculate it by substracting the addresses of two consequtive elements.
		create special.make_filled(e, 2)
		Result := c_diff(special.item_address(0), special.item_address(1))
	ensure
		Result > 0
	end

	size_of_default: NATURAL_32
		-- gets the size in byte of the default value of type.
	do
		Result := size_of(create {ELEMENT})
	end
feature {NONE} -- external
	c_diff(first: POINTER; second: POINTER) : NATURAL_32
	external
		"C++ inline"
	alias
		"return (EIF_NATURAL_32)((char*)$second - (char*)$first)"
	end
end

note
	description: "Represents a piece of memory, allocated on the device. Can be converted to Eiffel Array."
	author: "Alexey Kolesnichenko"
	date: "$19.06.2014"
	revision: "$Revision$"

class CUDA_DATA_HANDLE[T -> ANY create default_create end]
inherit
	REFACTORING_HELPER
	export {NONE}
		all
	end
	DISPOSABLE

create
	from_device_ptr,
	new_handle,
	new_handle_default,
	empty
create {CUDA_DATA_HANDLE}
	share_handle
feature{NONE} -- creation
	empty
	local
		sizer: CUSTOM_SIZE[T]
	do
		element_size:= sizer.size_of_default
		create dimensions.make_1d (0)
	end

	from_device_ptr (an_element_size: NATURAL_32; a_pointer: POINTER; a_size: DATA_DIMENSIONS)
		-- uses an initialized pointer to device memory to create handle.
	require
		positive_element_size: an_element_size > 0
		non_null_pointer: not a_pointer.is_default_pointer
	do
		element_size := an_element_size
		pointer := a_pointer
		dimensions := a_size
		own_memory := true
	end

	new_handle (an_element_size: NATURAL_32; a_size: DATA_DIMENSIONS)
		-- creates a new handle and allocates memory on devices,
		-- to store `a_size' of elements of specified type
	require
		positive_element_size: an_element_size > 0
	local
		cuda: CUDA_HELPER
		required_memory: MEMORY_UNIT
	do
		-- to ensure proper memory allocation.
		element_size := an_element_size
		dimensions := a_size
		create required_memory.from_bytes (total_size)
		from_device_ptr (an_element_size, cuda.cuda_allocate_memory (required_memory), a_size)
		--cuda.debug_print_available_memory
	end

	new_handle_default (a_size: DATA_DIMENSIONS)
	local
		sizer: CUSTOM_SIZE[T]
	do
		new_handle (sizer.size_of_default, a_size)
	end

	share_handle (an_element_size: NATURAL_32; a_pointer: POINTER; a_size: DATA_DIMENSIONS)
	do
		from_device_ptr (an_element_size, a_pointer, a_size)
		own_memory := false
	end

feature -- data
	element_size: NATURAL_32
		-- size in bytes of a single element

	pointer: POINTER
	 	-- pointer to the beginning of data in device memory.

	dimensions : DATA_DIMENSIONS
		-- dimensions of data

	x_dimension: INTEGER
		-- first dimension - is always present
	do
		Result := dimensions.dimension (0)
	ensure
		Result > 0
		Result = dimensions.dimension (0)
	end

	y_dimension: INTEGER
	do
		if dimensions.dimensions < 0 then
			Result := 0
		else
			Result := dimensions.dimension (1)
		end
	ensure
		Result >= 0
		dimensions.dimensions < 2 implies Result = 0
	end

	total_size: NATURAL_64
		-- total size in bytes of all elements.
	do
		Result := dimensions.total_dimension_size.to_natural_64 * element_size
	ensure
		Result = dimensions.total_dimension_size.to_natural_64 * element_size
	end

feature -- manipulation

	at (i: INTEGER): T
	require
		index_non_negative: i >= 0
		index_in_range:  i < x_dimension.to_integer_32
	do
		create Result
		cuda_copy_from_device_to_host (pointer + element_size.to_integer_32 * i,
			$Result, element_size)
	end

	subset_1d (start_pos, end_pos: INTEGER): like Current
		-- Returns a handle to a one-dimentional subset of current handle, which shares memory
		-- with the current handle.
		-- The new handle will not be capable of freeing it's memory
	require
		non_negative_start: start_pos >= 0
		end_in_range:  end_pos < dimensions.total_dimension_size
		non_empty_interval: end_pos - start_pos + 1 > 0
		single_dimension: dimensions.dimensions <= 2 -- for now, could be omitted in future
	local
		new_ptr: POINTER
		new_size: DATA_DIMENSIONS
	do
		new_ptr := pointer + element_size.to_integer_32 * start_pos
		create new_size.make_1d (end_pos - start_pos + 1)
		create Result.share_handle (element_size, new_ptr, new_size)
	ensure
		Result.x_dimension = end_pos - start_pos + 1
		Result.dimensions.dimensions = 1
	end

	free_memory
		-- frees the memory at the device side and sets the pointer to null.
		-- subsequent calls will do nothing.
	local
		cuda_h: CUDA_HELPER
	do
		-- if device memory is shared with this handle,
		-- it is not necessary to do a clean-up in this instance.
		-- the clean-up will happen at parent node.
		if own_memory then
			cuda_free_memory (pointer)

			if not pointer.is_default_pointer then
				pointer := create {POINTER}.default_create
				--cuda_h.debug_print_available_memory
			end
		end
	ensure
		pointer.is_default_pointer
	end

	to_special: SPECIAL[T]
	require
		not pointer.is_default_pointer
	do
		fixme ("TODO: eliminate excess initialization")
		create Result.make_filled (create {T}, dimensions.total_dimension_size)

		-- replacing dummy, prefilled data with actual data from device.
		cuda_copy_from_device_to_host (pointer, Result.base_address, total_size)
	end

	to_array: ARRAY[T]
	require
		not pointer.is_default_pointer
	local
		spec: SPECIAL[T]
	do
		create Result.make_from_special (to_special)
	ensure
		Result.count = dimensions.total_dimension_size
	end

	to_array2: ARRAY2[T]
	local
		d: DEVICE_HOST_CONVERTER[T]
	do
		create d
		Result := d.to_array2 (Current)
	end

feature -- disposable implementation
	dispose
	do
		free_memory
	end

feature{NONE} -- externals & internals

	own_memory: BOOLEAN

	cuda_free_memory (p: POINTER)
	external
		"C++ inline use %"cuda.h%", %"cuda_runtime.h%", %"cuda_device_runtime_api.h%""
	alias
		"[
			cudaFree($p);
		]"
    end

    cuda_copy_from_device_to_host (device_ptr: POINTER; host_ptr: POINTER; a_total_size: NATURAL_64)
	external
		"C++ inline use %"cuda.h%", %"cuda_runtime.h%", %"cuda_device_runtime_api.h%""
	alias
		"[
			cudaMemcpy($host_ptr, $device_ptr, $a_total_size, cudaMemcpyDeviceToHost);
		]"
    end
end

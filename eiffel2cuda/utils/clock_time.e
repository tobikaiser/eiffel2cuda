note
	description: "Wraps clock() from C. On Linux computes CPU clock time; on Windows - Wall time."
	author: "Alexey Kolesnichenko."
	date: "$Date$"
	revision: "$Revision$"

class CLOCK_TIME
feature
	clock: INTEGER_64
	external
		"C++ inline use <ctime>"
	alias
		"[
			return clock();
		]"
	end

	formatted_diff(last, first: INTEGER_64): STRING
	-- produces formatted time diff. Maintains 4 decimal places in the fractional part.
	do
		Result := formatted_double (diff(last, first))
	end

	formatted_double(duration: DOUBLE): STRING
	-- formats `duration' to string. Maintains 4 decimal places in the fractional part.
	local
		df: FORMAT_DOUBLE
	do
		create df.make (5, 4)
		Result := df.formatted (duration)
	end

	diff(last, first: INTEGER_64): DOUBLE
	external
		"C++ inline use <ctime>"
	alias
		"[
			return (double)($last - $first) / CLOCKS_PER_SEC;
		]"
	end
end

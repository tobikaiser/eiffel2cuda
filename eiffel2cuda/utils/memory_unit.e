note
	description: "Summary description for {MEMORY_UNIT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

expanded class MEMORY_UNIT
inherit ANY redefine default_create, out end
create
	from_bytes, from_kilobytes, from_megabytes, from_gigabytes, default_create
feature {NONE}
	from_bytes (a_bytes: NATURAL_64)
	do
		bytes:= a_bytes
	end

	from_kilobytes (a_kilobytes, a_bytes: NATURAL_64)
	require
		a_bytes < 1024
	do
		from_bytes (a_kilobytes * 1024 + a_bytes)
	end

	from_megabytes (a_megabytes, a_kilobytes, a_bytes: NATURAL_64)
	require
		a_kilobytes < 1024
		a_bytes < 1024
	do
		from_kilobytes (a_megabytes * 1024 + a_kilobytes, a_bytes)
	end

	from_gigabytes (a_gigabytes, a_megabytes, a_kilobytes, a_bytes: NATURAL_64)
	require
		a_megabytes < 1024
		a_kilobytes < 1024
		a_bytes < 1024
	do
		from_megabytes ( a_gigabytes * 1024 + a_megabytes, a_kilobytes, a_bytes)
	end

	default_create
	do

	end
feature
	bytes: NATURAL_64

	kilobytes: REAL_64
	do
		Result := bytes.to_real_64 / 1024
	end

	exact_kilobytes: NATURAL_64
	do
		Result := bytes // 1024
	end

	megabytes: REAL_64
	do
		Result := kilobytes / 1024
	end

	exact_megabytes: NATURAL_64
	do
		Result := exact_kilobytes // 1024
	end

	gigabytes: REAL_64
	do
		Result := megabytes / 1024
	end

	exact_gigabytes: NATURAL_64
	do
		Result := exact_megabytes // 1024
	end
feature
	out: STRING
	do
		Result := bytes.out
	end
end

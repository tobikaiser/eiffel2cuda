note
	description: "Summary description for {ARRAY_UTIL}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

expanded class
	ARRAY_UTIL
feature
	add_vectors (first: ARRAY[INTEGER]; second: ARRAY[INTEGER]) : ARRAY[INTEGER]
	require
		first.count = second.count
	local
		i: INTEGER
		size: INTEGER
	do
		size := first.count
		create Result.make_filled(0, 1, size)
		from
			i := 1
		until
			i = size + 1
		loop
			Result[i] := first[i] + second[i]
			i := i + 1
		end
	end

	multiply_matrices (first, second: ARRAY2[INTEGER]): ARRAY2[INTEGER]
	local
		i, j, k, sum: INTEGER
	do
		create Result.make_filled (0, first.height, second.width)

		from
			j := 1
		until
			j = first.width + 1
		loop
			from
				i := 1
			until
				i = second.height + 1
			loop
				sum := 0
				from
					k := 1
				until
					k = first.height + 1
				loop
					sum := sum + first[i, k]*second[k, j]
					k := k + 1
				end
				Result[i,j] := sum
				i := i + 1
			end
			j := j + 1
		end
	end

	create_random_vector (size: INTEGER): ARRAY[INTEGER]
	require
		size >= 1
	local
		i: INTEGER
		rand: C_RAND
	do
		create Result.make_filled (0, 1, size)
		create rand.default_create
		from
			i := 1
		until
			i = size + 1
		loop
			Result[i] := to_limit(rand.rand, 1000)
			i := i + 1
		end
	end

	create_random_double_vector (size: INTEGER): ARRAY[DOUBLE]
	require
		size >= 1
	local
		i: INTEGER
		rand: C_RAND
	do
		create Result.make_filled (0, 1, size)
		create rand.default_create
		from
			i := 1
		until
			i = size + 1
		loop
			Result[i] := rand.double_rand_in_interval (-5000, 10000)
			i := i + 1
		end
	end

	create_random_matrix (rows, columns: INTEGER): ARRAY2[INTEGER]
	local
		i, j: INTEGER
		rand: C_RAND
	do
		create Result.make_filled (0, rows, columns)
		from
			i := 1
		until
			i = rows + 1
		loop
			from
				j := 1
			until
				j = columns + 1
			loop
				Result[i, j] := to_limit(rand.rand, 10)
				j := j + 1
			end

			i := i + 1
		end

	end

	create_random_double_matrix (rows, columns: INTEGER): ARRAY2[DOUBLE]
	local
		i, j: INTEGER
		rand: C_RAND
	do
		create Result.make_filled (0, rows, columns)
		from
			i := 1
		until
			i = rows + 1
		loop
			from
				j := 1
			until
				j = columns + 1
			loop
				Result[i, j] :=  rand.double_rand_in_interval (-5000, 10000)
				j := j + 1
			end

			i := i + 1
		end
	end

	to_limit (num: INTEGER; limit: INTEGER) : INTEGER
	do
		Result := num \\ limit
	end

	print_array (ar: ITERABLE[ANY])
	do
		across ar as elem
		loop
			print(elem.item.out + " ")
		end

		print("%N")
	end

	print_matrix (ar: ARRAY2[ANY])
	local
		i, j: INTEGER
	do
		from
			i := 1
		until
			i = ar.height + 1
		loop
			from
				j := 1
			until
				j = ar.width + 1
			loop
				print(ar[i, j].out + " ")
				j := j + 1
			end
			print("%N")
			i:= i + 1
		end

		print("%N")
	end
feature -- simplified array creation
	one_element (e: ANY): ARRAY[like e]
	local
		ret: ARRAYED_LIST[like e]
	do
		create ret.make (1)
		ret.extend (e)
		Result := ret.to_array
	end

	two_elements (first: ANY; second: like first): ARRAY[like first]
	local
		ret: ARRAYED_LIST[like first]
	do
		create ret.make (2)
		ret.extend (first)
		ret.extend (second)
		Result := ret.to_array
	end

	three_elements (first: ANY; second, third: like first): ARRAY[like first]
	local
		ret: ARRAYED_LIST[like first]
	do
		create ret.make (3)
		ret.extend (first)
		ret.extend (second)
		ret.extend (third)
		Result := ret.to_array
	end

	four_elements (first: ANY; second, third, fourth: like first): ARRAY[like first]
	local
		ret: ARRAYED_LIST[like first]
	do
		create ret.make (4)
		ret.extend (first)
		ret.extend (second)
		ret.extend (third)
		ret.extend (fourth)
		Result := ret.to_array
	end

feature {NONE} -- C rand
	c_rand: INTEGER
	external
		"C++ inline use <ctime>, <cstdlib>"
	alias
		"[
  			return (time(NULL));
  			return rand();
		]"
	end
end

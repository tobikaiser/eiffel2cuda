note
	description: "Contains useful wrappers to measure the running time of actions/functions."
	author: "Alexey Kolesnichenko."
	date: "26.06.2014"
	revision: "$Revision$"

expanded class	ACTION_PROFILER

feature
	measure_time(action: ROUTINE[ANY, TUPLE]): DT_TIME_DURATION
		-- measures the running time of an action.
	local
		system_clock: DT_SHARED_SYSTEM_CLOCK
		start_time: DT_DATE_TIME
	do
		create system_clock
		start_time := system_clock.system_clock.date_time_now
		action.call ([])

		Result := (system_clock.system_clock.date_time_now - start_time)
	end

	report_action(action: ROUTINE[ANY, TUPLE]; action_description: STRING)
		-- prints the running time (measured in seconds) of the `action' with it's description.
	local
		duration: DT_TIME_DURATION

		df: FORMAT_DOUBLE
	do
		duration := measure_time (action)
		create df.make (5, 3)

		print(action_description + ": ")
		print( df.formatted (duration.millisecond_count) + " ms")
		print("%N")
	end

	report_function(f: FUNCTION[ANY, TUPLE, ANY]; action_description: STRING): FUNC_RESULT[like f]
		-- prints the running time (measured in seconds)
		-- of the function `f' with it's description and returns the function's result.
	local
		l_item: like report_function.item
	do
		report_action (f, action_description)
		if attached f.last_result as ret then
			l_item := ret
		else
			-- should not happen.
			l_item := f.item ([])
		end
		create Result.make(f, l_item)
	end

feature{NONE}
	floating_seconds(milliseconds: INTEGER): DOUBLE
	do
		Result := (milliseconds.as_natural_32 / 1000)
	end
end

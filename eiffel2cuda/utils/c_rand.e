note
	description: "Encapsulates rand() from C library"
	author: "Alexey Kolesnichenko"
	date: "24.06.2014"
	revision: "$Revision$"

expanded class C_RAND
inherit ANY
	redefine
		default_create
	end
create
	default_create
feature {NONE}
	default_create
	do
		seed_to_current_time
	end
feature
	seed_to_current_time
	do
		c_srand
	end

	rand: INTEGER
	do
		Result := c_rand
	end

	rand_max: INTEGER_64
	-- maximum possible random integer value
	do
		Result := c_rand_max
	end

	rand_in_interval (min, max: INTEGER): INTEGER
	-- unbiased uniform distribution from interval [min, max] , by rejection sampling
	-- adapted from `http://stackoverflow.com/questions/11758809'
	require
		non_empty_inteval: min < max
	local
		remainder: INTEGER_64
		n, x: INTEGER
	do
 		n := max - min + 1
    	remainder := rand_max \\ n

    	from
    		x := rand
    	until
    		x < rand_max - remainder
    	loop
    		x := rand
    	end

    	Result := min + x \\ n;
	end

	double_rand: DOUBLE
	-- next random floating point from interval [0, 1)
	do
		Result := c_double_rand
	end

	double_rand_in_interval (min, max: DOUBLE): DOUBLE
	-- next random floating point from interval [min, max)
	require
		non_empty_inteval: min < max
	do
		Result := (max - min) * double_rand + min
	end

feature {NONE} -- c wrappers
	c_srand
	external
		"C++ inline use <cstdlib>, <ctime>"
	alias
		"srand(clock())"
	end

	c_rand: INTEGER
	external
		"C++ inline use <cstdlib>"
	alias
		"return rand()"
	end

	c_double_rand: DOUBLE
	external
		"C++ inline use <cstdlib>"
	alias
		"return ((double)rand() /(double)RAND_MAX)"
	end

	c_rand_max: INTEGER_64
	external
		"C++ inline use <cstdlib>"
	alias
		"return RAND_MAX"
	end
end

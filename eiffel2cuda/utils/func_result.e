note
	description: "A hacky class to cirqumvent the impossibility to anchor a local variable's chain."
	author: "Alexey Kolesnichenko"
	date: "26.06.2014"
	revision: "$Revision$"

class
	FUNC_RESULT[T -> FUNCTION[ANY, TUPLE, ANY]]

create {ACTION_PROFILER}
	make
feature {NONE}
	make(a_func: T; a_item: like item)
	do
		func := a_func
		item := a_item
	end
feature
	item: like func.item
feature {NONE}
	func : T
end

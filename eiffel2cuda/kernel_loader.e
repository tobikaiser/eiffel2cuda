note
	description: "Summary description for {KERNEL_LOADER}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

expanded class KERNEL_LOADER
	inherit ANY
	redefine
		default_create
	end
create
	default_create, force_compile
feature {NONE} -- creation
	default_create
	do
		-- do nothing
	end

	force_compile
	do
		always_compile := TRUE
	end
feature -- API

	-- todo: move out of loader
	kernel_to_file(kernel_template: KERNEL_TEMPLATE; file_name: STRING): STRING
	local
		cu_file: PLAIN_TEXT_FILE
		name_base: STRING
	do
		name_base := file_name + "."
		create cu_file.make_with_name (name_base + "cu")

		if not cu_file.exists then
			create cu_file.make_open_write (name_base + "cu")
			cu_file.putstring (kernel_template.generate_kernel)
			cu_file.close
		end

		Result := name_base + "ptx"
	end

	load_kernel(ptx_file_name: STRING; kernel_name: STRING): POINTER
	require
		--(create {PLAIN_TEXT_FILE}.make_open_read (ptx_file_name)).exists
	local
		c_ptx_file_name: C_STRING
		c_kernel_name: C_STRING
		ignore: STRING
	do
		ignore := prepare_ptx_file (create {PLAIN_TEXT_FILE}.make_open_read (cu_file_name (ptx_file_name)))
		create c_ptx_file_name.make (ptx_file_name)
		create c_kernel_name.make (kernel_name)
		Result := c_load_kernel (c_ptx_file_name.item, c_kernel_name.item)
	end
feature -- Settings
	always_compile: BOOLEAN
feature{NONE}
	cu_file_name(file_name: STRING): STRING
	require
		--(create {PLAIN_TEXT_FILE}.make_open_read (file_name)).exists
	local
		end_index: INTEGER
	do
		if file_name.ends_with (".cu") then
			Result:= file_name
		else
			end_index := file_name.last_index_of ('.', file_name.count)

    		if (end_index = 0) then
    			end_index := file_name.count - 1
    		end

			-- replacing the extension.
    		Result := (file_name.substring (1, end_index) + "cu").out;
		end
	end

	c_load_kernel(ptx_file_name: POINTER; kernel_name: POINTER) : POINTER
	external
		"C++ inline use <iostream>, %"cuda.h%", %"cuda_runtime.h%", %"cuda_device_runtime_api.h%""
	alias
		"[
			CUmodule module;
			CUfunction function;

			const char* module_file = (const char*)$ptx_file_name;
			const char* kernel_name = (const char*)$kernel_name;
			CUresult err;
			
			err = cuModuleLoad(&module, module_file);

			if (err != CUDA_SUCCESS) {
				std::cerr<<"cannot find module " << module_file <<std::endl;
				const char* errName;
				const char* errDescr;
				CUresult queryErrDescr = cuGetErrorString(err, &errDescr);
				CUresult queryErrName = cuGetErrorName(err, &errName);
				if (!queryErrDescr && !queryErrName) {
					std::cerr << "\"" << errName << "\": " <<errDescr<<std::endl;
				}
				exit(-1);
			}
			
			err = cuModuleGetFunction(&function, module, kernel_name);
			if (err != CUDA_SUCCESS) {
				std::cerr<<"cannot load kernel function: " << kernel_name <<std::endl;
				exit(-1);
			}
			
			return function;
		]"
	end

    prepare_ptx_file(cu_file: PLAIN_TEXT_FILE): STRING
     -- The extension of the given file name is replaced with "ptx".
     -- If the file with the resulting name does not exist, it is
     -- compiled from the given file using NVCC. The name of the
     -- PTX file is returned.
     --
     -- `cuFileName' The .CU file
    require
    	cu_file.exists
    local
    	end_index: INTEGER
		ptx_file: PLAIN_TEXT_FILE
		model_string: STRING
		command: STRING
		l_cu_file_name: STRING
		cuda_h: CUDA_HELPER
    do
    	l_cu_file_name := cu_file.path.name.to_string_32
    	end_index := l_cu_file_name.last_index_of ('.', l_cu_file_name.count)

    	if (end_index = 0) then
    		end_index := l_cu_file_name.count - 1
    	end

		-- replacing the extension.
    	Result := (l_cu_file_name.substring (1, end_index) + "ptx").out;
    	create ptx_file.make_with_name(Result)

    	if (not ptx_file.exists or always_compile) then
			model_string := "-m64" + " -arch=" + cuda_h.compute_capability.to_compute_sm
			command:= "nvcc " + model_string + " -ptx " + cu_file.path.name + " -o " + ptx_file.path.name

			io.error.put_string("Executing%N" + command + "%N")

			compile_ptx (command)
    	end
    end

    compile_ptx(command: STRING)
    local
    	process_factory: PROCESS_FACTORY
    	nvcc_process: PROCESS
    do
		create process_factory
		nvcc_process := process_factory.process_launcher_with_command_line (command, VOID)
		nvcc_process.launch
		nvcc_process.wait_for_exit
    end

--    {
--        int endIndex = cuFileName.lastIndexOf('.');
--        if (endIndex == -1) {
--            endIndex = cuFileName.length() - 1;
--        }

--        String ptxFileName = cuFileName.substring(0, endIndex + 1) + "ptx";
--        File ptxFile = new File(ptxFileName);

--        if (ptxFile.exists()) {
--            return ptxFileName;
--        }

--        File cuFile = new File(cuFileName);
--        if (!cuFile.exists()) {
--            throw new IOException("Input file not found: " + cuFileName);
--        }

--        String modelString = "-m" + System.getProperty("sun.arch.data.model");
--        String command = "nvcc " + modelString + " -ptx " + cuFile.getPath() + " -o " + ptxFileName;

--        System.out.println("Executing\n" + command);
--        Process process = Runtime.getRuntime().exec(command);

--        String errorMessage = new String(toByteArray(process.getErrorStream()));
--        String outputMessage = new String(toByteArray(process.getInputStream()));
--        int exitValue = 0;
--        try {
--            exitValue = process.waitFor();
--        } catch (InterruptedException e) {
--            Thread.currentThread().interrupt();
--            throw new IOException("Interrupted while waiting for nvcc output", e);
--        }

--        if (exitValue != 0) {
--            System.out.println("nvcc process exitValue " + exitValue);
--            System.out.println("errorMessage:\n" + errorMessage);
--            System.out.println("outputMessage:\n" + outputMessage);
--            throw new IOException("Could not create .ptx file: " + errorMessage);
--        }

--        System.out.println("Finished creating PTX file");
--        return ptxFileName;
--    }
end

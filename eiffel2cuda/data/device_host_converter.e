note
	description: "Converts data from device to host."
	author: "Alexey Kolesnichenko"
	date: "30.06.2014"
	revision: "$Revision$"

class DEVICE_HOST_CONVERTER[T -> ANY create default_create end]
feature -- array support

	to_array (handle: CUDA_DATA_HANDLE[T]): ARRAY[T]
	do
		Result := handle.to_array
	end

	to_array2 (handle: CUDA_DATA_HANDLE[T]): ARRAY2[T]
	require
		handle.dimensions.dimensions > 1
	local
		ret: ARRAY2_EX[T]
	do
		create ret.make_from_area(handle.to_special,
			handle.dimensions.dimension(0),
			handle.dimensions.dimension(1))

		if attached {ARRAY2[T]} ret as r then
			Result:= r
		else
			Result := ret
		end
	end
end


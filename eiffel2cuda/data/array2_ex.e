note
	description: "Extended version of ARRAY2, which can be created from already filled buffer."
	author: "Alexey Kolesnichenko"
	date: "30.06.2014"
	revision: "$Revision$"

class	ARRAY2_EX[T]
	inherit ARRAY2[T]

create make_from_area
feature
	make_from_area(an_area: SPECIAL[T]; rows, columns: INTEGER)
	require
		rows > 0
		columns > 0
		an_area.count = rows * columns
	do
		make_from_special (an_area)
		height := rows
		width := columns
	end
end

note
	description: "A helper class, provides a low-level API to CUDA. Mostly memory management"
	author: "Alexey Kolesnichenko."
	date: "24.06.2014"
	revision: "$Revision$"

expanded class CUDA_HELPER
feature
	cuda_allocate_memory(memory_unit: MEMORY_UNIT): POINTER
	require
		enough_memory: memory_unit.bytes <= free_device_memory.bytes
	do
		Result := cuda_allocate_bytes (memory_unit.bytes)
	end

	cuda_calloc_memory(memory_unit: MEMORY_UNIT): POINTER
	require
		enough_memory: memory_unit.bytes <= free_device_memory.bytes
	do
		Result := cuda_allocate_memory (memory_unit)
		cuda_memset (Result, 0, memory_unit.bytes)
	end

	total_device_memory: MEMORY_UNIT
	do
		create Result.from_bytes (c_total_device_memory)
	end

	free_device_memory: MEMORY_UNIT
	do
		create Result.from_bytes (c_free_device_memory)
	end

	max_shared_memory: MEMORY_UNIT
	do
		create Result.from_bytes (c_max_shared_memory)
	end

	compute_capability: COMPUTE_CAPABILITY
	do
		create Result.make (c_compute_major, c_compute_minor)
	end

feature
	debug_print_available_memory
	do
		io.error.put_string ("Available device memory: " + free_device_memory.megabytes.out + " Mb.%N")

	end

feature {NONE}
	cuda_memset (ptr: POINTER; value: INTEGER_8; bytes: NATURAL_64)
	external
		"C++ inline use <iostream>, %"cuda.h%""
	alias
		"[
			cudaError_t error = cudaMemset($ptr, $value, $bytes);
			if (error) {
				std::cerr<<"cannot set CUDA memory to "<< $value <<std::endl;
				exit(-1);
			}
		]"
	end

	cuda_allocate_bytes (bytes: NATURAL_64): POINTER
		-- allocates the specified amount `size' bytes at the device side.
	external
		"C++ inline use <iostream>, %"cuda.h%", %"cuda_device_runtime_api.h%", %"cuda_runtime_api.h%""
	alias
		"[
			void* cudaMem;
			cudaError_t error = cudaMalloc((void **) &cudaMem, $bytes);
			if (error) {
				std::cerr<<"cannot allocate CUDA memory"<<std::endl;
				exit(-1);
			}

			return cudaMem;
		]"
	end

	c_compute_minor: INTEGER
	external
		"C++ inline use <iostream>, %"cuda.h%", %"cuda_device_runtime_api.h%", %"cuda_runtime_api.h%""
	alias
		"[
			cudaDeviceProp prop;
			cudaError_t err = cudaGetDeviceProperties(&prop, 0);
			if (err) {
				std::cerr<<"cannot query device properties"<<std::endl;
				exit(-1);
			}
			return prop.minor;
		]"
	end

	c_compute_major: INTEGER
	external
		"C++ inline use <iostream>, %"cuda.h%", %"cuda_device_runtime_api.h%", %"cuda_runtime_api.h%""
	alias
		"[
			cudaDeviceProp prop;
			cudaError_t err = cudaGetDeviceProperties(&prop, 0);
			if (err) {
				std::cerr<<"cannot query device properties"<<std::endl;
				exit(-1);
			}
			return prop.major;
		]"
	end

	c_max_shared_memory: NATURAL_64
	external
		"C++ inline use <iostream>, %"cuda.h%", %"cuda_device_runtime_api.h%", %"cuda_runtime_api.h%""
	alias
		"[
			cudaDeviceProp prop;
			cudaError_t err = cudaGetDeviceProperties(&prop, 0);
			if (err) {
				std::cerr<<"cannot query device properties"<<std::endl;
				exit(-1);
			}
			return prop.sharedMemPerBlock;
		]"
	end

	c_total_device_memory: NATURAL_64
		-- queries total amount of device memory
	external
		"C++ inline use <iostream>, %"cuda.h%", %"cuda_device_runtime_api.h%", %"cuda_runtime_api.h%""
	alias
		"[
			size_t total, free;
			cudaError_t err = cudaMemGetInfo(&free, &total);
			if (err) {
				std::cerr<<"cannot query total device memory"<<std::endl;
				exit(-1);
			}
			
			return total;
		]"
	end

	c_free_device_memory: NATURAL_64
		-- queries total amount of device memory
	external
		"C++ inline use <iostream>, %"cuda.h%", %"cuda_device_runtime_api.h%""
	alias
		"[
			size_t total, free;
			cudaError_t err = cudaMemGetInfo(&free, &total);
			if (err) {
				std::cerr<<"cannot query free device memory"<<std::endl;
				exit(-1);
			}
			return free;
		]"
	end
end

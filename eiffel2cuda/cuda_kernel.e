note
	description: "Represents a cuda kernel. Incapsulates input and output data."
	author: "Alexey Kolesnichenko"
	date: "19.06.2014"
	revision: "$Revision$"

class CUDA_KERNEL [OUT -> ANY create default_create end]

create
	make
feature {NONE} -- creation

	make (a_input: ARRAY[CUDA_DATA_HANDLE[ANY]]; an_output: CUDA_DATA_HANDLE[OUT])
	require
		valid_input: is_input_initialized(a_input)
		valid_output: not an_output.pointer.is_default_pointer and a_input.has (an_output)
	do
		input := a_input
		output := an_output
	end

feature -- launching and control
	load_kernel (ptx_file_name, kernel_name: STRING)
	require
		file_name_not_empty: not ptx_file_name.is_empty
		kernel_name_not_empty: not kernel_name.is_empty
	local
		kernel_loader: KERNEL_LOADER
	do
		create kernel_loader.default_create
		kernel_ptr := kernel_loader.load_kernel (ptx_file_name, kernel_name)
	ensure
		not kernel_ptr.is_default_pointer
	end

	launch (grid_dim: DIM3; block_dim: DIM3)
		-- synchronous launch.
	do
		launch_shared_memory (grid_dim, block_dim, {NATURAL} 0)
	end

	launch_shared_memory (grid_dim: DIM3; block_dim: DIM3; shared_mem_size: NATURAL_32)
	do
		c_launch_kernel (
			kernel_ptr, pack_parameters(input),
			total_params (input),
			grid_dim.x, grid_dim.y, grid_dim.z,
			block_dim.x, block_dim.y, block_dim.z,
			shared_mem_size)
	end

	launch_with_arguments (a_launch_params: KERNEL_LAUNCH_PARAMS;
		a_input: ARRAY[CUDA_DATA_HANDLE[ANY]];
		an_output: CUDA_DATA_HANDLE[OUT]) : like an_output
	require
		valid_output: not an_output.pointer.is_default_pointer and a_input.has (an_output)
	do
		c_launch_kernel (
			kernel_ptr, pack_parameters(a_input),
			total_params (a_input),
			a_launch_params.grid.x, a_launch_params.grid.y, a_launch_params.grid.z,
			a_launch_params.block.x, a_launch_params.block.y, a_launch_params.block.z,
			a_launch_params.shared_memory_size)

		Result := an_output
	end

feature -- data
	input: ARRAY[CUDA_DATA_HANDLE[ANY]]
	output: CUDA_DATA_HANDLE[OUT]

feature {NONE} -- implementation
	kernel_ptr: POINTER

	is_input_initialized (a_input: ITERABLE[CUDA_DATA_HANDLE[ANY]]): BOOLEAN
		-- all handles have non-null pointers?
	do
		Result := across a_input as handle all not handle.item.pointer.is_default_pointer end
	end

	create_parameter_package (params_count: INTEGER): POINTER
	external
		"C++ inline"
	alias
		"[
			void** a = new void*[$params_count];
			return a;
		]"
	end

	pack_parameters(a_parameters: like input) : POINTER
	local
		i, cur_package: INTEGER
		package_base: POINTER
		l_total_params: like total_params
	do
		l_total_params := total_params (a_parameters)
		package_base := create_parameter_package (l_total_params)
		from
			i := 0; cur_package := input.lower
		until
			cur_package = a_parameters.upper + 1
		loop
			i := pack_parameter (package_base, i, a_parameters[cur_package])
			cur_package := cur_package + 1
		end

		--print("packed " + i.out + " param pairs%N")
		Result := package_base
	end

	total_params (a_parameters: like input): INTEGER
	local
		i: INTEGER
	do
		Result := a_parameters.count
		from
			i := a_parameters.lower
		until
			i = a_parameters.upper + 1
		loop
			Result := Result + a_parameters[i].dimensions.dimensions
			i := i + 1
		end
	end

	pack_parameter (package_base: POINTER; current_index: INTEGER; parameter: CUDA_DATA_HANDLE[ANY]): INTEGER
		-- packs parameter -- it's data and dimensions and
		--returns the next vacant index in the parameter package
	local
		d: INTEGER
	do
		Result := current_index

		c_pack_data (package_base, Result, parameter.pointer)

		Result := Result + 1
		from
			d := 0
		until
			d = parameter.dimensions.dimensions
		loop
			c_pack_dimension (package_base, Result, parameter.dimensions.dimension (d))
			d := d + 1
			Result := Result + 1
		end
	end

	c_pack_data (package_base: POINTER; index: INTEGER; data: POINTER )
	external
		"C++ inline"
	alias
		"[
			void** package = (void**)$package_base;
			package[$index] = $data;
		]"
	end

	c_pack_dimension (package_base: POINTER; index, dim: INTEGER )
	external
		"C++ inline"
	alias
		"[
			void** package = (void**)$package_base;
			package[$index] = (void*)$dim;
		]"
	end

	c_launch_kernel (
		a_kernel_ptr: POINTER
		params: POINTER; params_count: INTEGER;
		grid_x, grid_y, grid_z,
		block_x, block_y, block_z: NATURAL_32;
		shared_mem: NATURAL_32)
	external
		"C++ inline use <iostream>, <vector>, %"cuda.h%", %"cuda_runtime_api.h%""
	alias
		"[
			CUresult err;		
			
			std::vector<void*> raw_params((void**)$params, (void**)$params + $params_count);
			std::vector<void*> param_vec(raw_params.size());
			
			for (size_t i = 0; i < raw_params.size(); i++) {
				param_vec[i] = &raw_params[i];
			}	
			
			err = cuLaunchKernel(
				(CUfunction)$a_kernel_ptr,	
				$grid_x,	
				$grid_y,	
				$grid_z,	
				$block_x, 
				$block_y, 
				$block_z, 
				$shared_mem,
				NULL, 
				&param_vec[0],
				NULL);
			
			if (err != CUDA_SUCCESS) {
				const char* errName;
				const char* errDescr;
				CUresult queryErrDescr = cuGetErrorString(err, &errDescr);
				CUresult queryErrName = cuGetErrorName(err, &errName);
				if (!queryErrDescr && !queryErrName) {
					std::cerr << "\"" << errName << "\": " <<errDescr<<std::endl;
				}
				
				std::cerr<<"cannot launch kernel function." <<std::endl;
				exit(-1);
			}
			cudaDeviceSynchronize();
		]"
	end

feature -- Disposable implementation
	dispose
	do
		across input as handle loop handle.item.dispose end
		output.dispose
	end
end

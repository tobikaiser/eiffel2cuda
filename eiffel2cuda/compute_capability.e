note
	description: "Represents a compute capability (which indicates, what features are available) of a cuda device."
	author: "Alexey Kolesnichenko"
	date: "4.07.2014"
	revision: "$Revision$"

expanded class COMPUTE_CAPABILITY
inherit ANY
	redefine
		default_create, out
	end
create
	default_create, make
feature {NONE}
	make(a_major, a_minor: INTEGER)
	require
		a_major > 0
		a_minor >= 0
	do
		major := a_major
		minor := a_minor
	end

	default_create
	do
		-- assuming the least advanced compute capability.
		make (1, 0)
	end
feature

	major: INTEGER
	minor: INTEGER

	out: STRING
	do
		Result := major.out + "." + minor.out
	end

	to_compute_sm: STRING
	do
		if major >=3 then
			Result := "compute_30"
		else
			if major = 2  then
				Result := "compute_20"
			else
				-- todo: maybe 1.3 would suit better
				Result := "compute_10"
			end
		end
	end
end

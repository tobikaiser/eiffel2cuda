note
	description: "Summary description for {KERNEL_LAUNCH_PARAMS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class KERNEL_LAUNCH_PARAMS
create
	make, make_with_shared_memory
feature {NONE}
	make (a_grid, a_block: DIM3)
	do
		make_with_shared_memory (a_grid, a_block, 0)
	end

	make_with_shared_memory (a_grid: DIM3; a_block: DIM3; a_shared_memory_size: NATURAL_32)
	do
		grid := a_grid
		block := a_block
		shared_memory_size := a_shared_memory_size
	end
feature
	grid: DIM3
	block: DIM3
	shared_memory_size: NATURAL_32
end

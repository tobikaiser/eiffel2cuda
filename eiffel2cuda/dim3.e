note
	description: "3-d dimentional grid setup for convienience of interaction with CUDA."
	author: "Alexey Kolesnichenko"
	date: "23.06.2014"
	revision: "$Revision$"

class DIM3
inherit
	ANY
	redefine
		out
	end
create {GRID_FACTORY}
	make_1d
create
	make, make_2d
feature {NONE}
	make(xx: NATURAL_32; yy: NATURAL_32; zz: NATURAL_32)
	require
		xx > 0
		yy > 0
		zz > 0
	do
		x := xx
		y := yy
		z := zz
	end

	make_2d(xx, yy: NATURAL_32)
	require
		xx > 0
		yy > 0
	do
		make(xx, yy, 1)
	end

	make_1d(xx: NATURAL_32)
	require
		xx > 0
	do
		make (xx, 1, 1)
	end
feature -- dimensions
	x: NATURAL_32
	y: NATURAL_32
	z: NATURAL_32

feature -- toString
	out: STRING
	do
		Result := x.out + " " + y.out + " " + z.out
	end
end

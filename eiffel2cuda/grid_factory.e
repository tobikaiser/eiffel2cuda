note
	description: "A factory to create proper grid structure for CUDA devices."
	author: "Alexey Kolesnichenko."
	date: "14.07.2014"
	revision: "$Revision$"

class GRID_FACTORY
feature -- shared
	warp_size: NATURAL_32 = 32
		-- currently is fixed on every hardware
	default_block_size: NATURAL_32 = 128
		-- default block_size

	is_valid_block_size (a_size: NATURAL_32): BOOLEAN
		-- checks the lower bound of a block size. todo: check the block size
	do
		Result := a_size > 0 and then a_size \\ warp_size = 0
	end
feature -- 1d grids
	default_block_dim_1d: DIM3
	attribute
		Result := block_dim_1d (default_block_size)
	end

	block_dim_1d (a_size: NATURAL_32): DIM3
	require
		is_valid_block_size (a_size)
	do
		create Result.make_1d (a_size)
	end

	default_grid_dim_1d (an_input_size: NATURAL_32) : DIM3
	do
		Result := grid_dim_1d (an_input_size, default_block_dim_1d)
	end

	grid_dim_1d (an_input_size: NATURAL_32; a_block: DIM3): DIM3
	require
		an_input_size > 0
	do
		if an_input_size \\ a_block.x  = 0 then
			create Result.make_1d (an_input_size // a_block.x)
		else
			create Result.make_1d (an_input_size // a_block.x + 1)
		end
	ensure
		input_fully_covered: Result.x * a_block.x >= an_input_size
		input_is_precisely_covered_if_possible:
			an_input_size \\ a_block.x  = 0	implies Result.x * a_block.x = an_input_size
	end

feature -- 2d grids
feature {NONE} -- implementation
end

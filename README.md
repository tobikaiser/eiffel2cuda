### What is this repository for? ###

This is SafeGPU (formerly - eiffel2cuda) - a framework for safe and efficient GPGPU (at the moment only CUDA backend is supported) programming in Eiffel. A programmer is not exposed to low-level details, rather she operates with common collection classes.
For example, vector addition could be simply written as "a + b" in SafeGPU!

The library is currently under active development, API is subject to change.
### How do I get set up? ###

To start using SafeGPU:

* Get EiffelStudio from dev.eiffel.com
* Install latest NVIDIA SDK toolkit from NVIDIA site
* Make sure, that environmental variable CUDA_PATH exists and points to the installation folder of NVIDIA SDK
* Try out SafeGPU! You can start with looking at examples or benchmarks folder


### Who do I talk to? ###

If you have any questions, please do not hesitate to contact Alexey - alexey.kolesnichenko@inf.ethz.ch

### Disclaimer ###
This software is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY. Use it at your own risk.
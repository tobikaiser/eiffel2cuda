note
	description : "Vector Addition Benchmark. GPU implementation"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

feature
	clock_timer: CLOCK_TIME
	attribute
		create Result
	end

	make
	local
		ar_util: ARRAY_UTIL
		first_data, second_data: ARRAY2[REAL_64]
		first, second, mult: G_MATRIX[REAL_64]
		rows, columns: INTEGER
		start: INTEGER_64
	do
		rows := argument (1).to_integer_32
		columns := rows
		
		first_data := ar_util.create_random_double_matrix (rows, columns)
		second_data := ar_util.create_random_double_matrix (rows, columns)
		
		start := clock_timer.clock
		create first.from_array2(first_data);
		create second.from_array2(second_data);
		
		mult := mat_mul(first, second)

		print (clock_timer.formatted_diff(clock_timer.clock, start) + "%N")

		if mult.rows = 0 then
			print ("")
		end
	end
	
	mat_mul(first, second: G_MATRIX[REAL_64]): G_MATRIX[REAL_64]
	require
		first.columns = second.rows
	do
		Result := first.multiply(second)
	end
end

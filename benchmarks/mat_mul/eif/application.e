note
	description : "Vector Addition Benchmark. GPU implementation"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

feature
	clock_timer: CLOCK_TIME
	attribute
		create Result
	end

	make
	local
		ar_util: ARRAY_UTIL
		first, second: ARRAY2 [REAL_64]
		cpu_result: ARRAY2 [REAL_64]
		rows, columns: INTEGER
		start: INTEGER_64
	do
		rows := argument (1).to_integer_32
		columns := rows

		first := ar_util.create_random_double_matrix (rows, columns)
		second := ar_util.create_random_double_matrix (rows, columns)

		start := clock_timer.clock
		cpu_result := multiply_matrices (first, second)

		print (clock_timer.formatted_diff(clock_timer.clock, start) + "%N")

		if cpu_result.count = 0 then
			print ("b")
		end
	end

	multiply_matrices (first, second: ARRAY2[REAL_64]): ARRAY2[REAL_64]
	local
		i, j, k: INTEGER
		sum: REAL_64
	do
		create Result.make_filled (0, first.height, second.width)

		from
			j := 1
		until
			j = first.width + 1
		loop
			from
				i := 1
			until
				i = second.height + 1
			loop
				sum := 0.0
				from
					k := 1
				until
					k = first.height + 1
				loop
					sum := sum + first[i, k]*second[k, j]
					k := k + 1
				end
				Result[i,j] := sum
				i := i + 1
			end
			j := j + 1
		end
	end
end

note
	description : "Vector Addition Benchmark. GPU implementation"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

feature
	clock_timer: CLOCK_TIME
	attribute
		create Result
	end
	make
	local
		ar_util: ARRAY_UTIL
		first_array, second_array: ARRAY[INTEGER]
		first, second, sum: G_VECTOR[INTEGER]
		count: INTEGER
		start: INTEGER_64
	do
		count := argument (1).to_integer_32

		first_array := ar_util.create_random_vector (count)
		second_array := ar_util.create_random_vector (count)

		start:= clock_timer.clock
		create first.from_array (first_array)
		create second.from_array (second_array)

		sum := g_sum(first, second)

		print (clock_timer.formatted_diff(clock_timer.clock, start) + "%N")

		if sum.count = 0 then
			print ("b")
		end
	end

	g_sum(a, b: G_VECTOR[INTEGER]): G_VECTOR[INTEGER]
	require
		a.count = b.count
	do
		Result := a + b
	ensure
		Result.count = a.count
	end
end

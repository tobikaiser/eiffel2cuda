note
	description : "Vector Addition Benchmark. GPU implementation"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

feature
	clock_timer: CLOCK_TIME
	attribute
		create Result
	end

	make
	local
		ar_util: ARRAY_UTIL
		first, second, sum: ARRAY[INTEGER]
		count: INTEGER
		start: INTEGER_64
	do
		count := argument (1).to_integer_32

		first := ar_util.create_random_vector (count)
		second := ar_util.create_random_vector (count)

		start := clock_timer.clock
		sum := cpu_sum(first, second)

		print (clock_timer.formatted_diff(clock_timer.clock, start) + "%N")

		if sum.count = 0 then
			print ("b")
		end
	end

	cpu_sum (a, b: ARRAY[INTEGER]): ARRAY[INTEGER]
	require
		a.count = b.count
	local
		i: INTEGER
	do
		create Result.make_filled (0, a.lower, a.upper)
		from
			i := 1
		until
			i = a.count
		loop
			Result[i] := a[i] + b[i]
			i := i + 1
		end
	ensure
		Result.count = a.count
	end


end

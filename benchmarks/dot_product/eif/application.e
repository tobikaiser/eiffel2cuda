note
	description : "Vector Addition Benchmark. GPU implementation"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

feature
	clock_timer: CLOCK_TIME
	attribute
		create Result
	end
	
	make
	local
		ar_util: ARRAY_UTIL
		first, second: ARRAY[DOUBLE]
		product: DOUBLE
		count: INTEGER
		start: INTEGER_64
	do
		count := argument (1).to_integer_32

		first := ar_util.create_random_double_vector (count)
		second := ar_util.create_random_double_vector (count)

		start := clock_timer.clock
		product := cpu_sum(first, second)

		print (clock_timer.formatted_diff(clock_timer.clock, start) + "%N")

		if product = 0.0 then
			print ("b")
		end
	end

	cpu_sum (a, b: ARRAY[DOUBLE]): DOUBLE
	require
		a.count = b.count
	local
		i: INTEGER
	do
		Result := 0.0
		from
			i := 1
		until
			i = a.count
		loop
			Result := Result + a[i] * b[i]
			i := i + 1
		end
	end
end

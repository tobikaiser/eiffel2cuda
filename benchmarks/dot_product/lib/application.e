note
	description : "Vector Addition Benchmark. GPU implementation"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

feature
	clock_timer: CLOCK_TIME
	attribute
		create Result
	end

	make
	local
		ar_util: ARRAY_UTIL
		first_array, second_array: ARRAY[REAL_32]
		first, second: G_VECTOR[REAL_32]
		product: DOUBLE
		count: INTEGER
		start: INTEGER_64
	do
		count := argument (1).to_integer_32

		first_array := ar_util.create_random_float_vector (count)
		second_array := ar_util.create_random_float_vector (count)

		start := clock_timer.clock
		create first.from_array (first_array)
		create second.from_array (second_array)

		product := g_product(first, second)

		print (clock_timer.formatted_diff(clock_timer.clock, start) + "%N")

		if product = 0 then
			print ("b")
		end
	end

	g_product(a, b: G_VECTOR[REAL_32]): REAL_32
	require
		a.count = b.count
	do
		Result := a.compwise_multiplication (b).sum
	end
end

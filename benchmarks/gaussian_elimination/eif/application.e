note
	description: "Summary description for {GAUSSIAN_ELIMINATION_EXAMPLE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class APPLICATION
inherit
	ARGUMENTS
create
	make
feature
	clock_timer: CLOCK_TIME
	attribute
		create Result
	end


	cpu_det: DOUBLE
	make
	local
		matrix: ARRAY2[DOUBLE]
		ar_util: ARRAY_UTIL
		rows, columns: INTEGER
		start: INTEGER_64
	do
		rows := argument (1).to_integer_32
		columns := rows
		matrix := ar_util.create_random_double_matrix (rows, columns)
		start := clock_timer.clock
		compute_det (matrix)

		print (clock_timer.formatted_diff(clock_timer.clock, start) + "%N")

		if cpu_det = 0.0 then
			io.error.put_string ("")
		end
	end

	compute_det (matrix: ARRAY2[DOUBLE])
	do
		cpu_det := det(matrix)
	end


	det (matrix: ARRAY2[DOUBLE]): DOUBLE
	require
		is_square: matrix.height = matrix.width
	local
		step, pivot_row, i, row, times_rows_swapped: INTEGER
		math: DOUBLE_MATH
		temp, pivot_element: DOUBLE
	do
		Result := 1.0
		create math
		from
			step := 1
		until
			step = matrix.height + 1
		loop
			pivot_row := step
			from
				i:= step + 1
			until
				i = matrix.height + 1
			loop
				if math.dabs (matrix[i, step]) > math.dabs (matrix[pivot_row, step]) then
					pivot_row := i
				end

				i := i + 1
			end

			if pivot_row /= step then
				-- swap rows
				from
					i := 1
				until
					i = matrix.width + 1
				loop
					temp := matrix[pivot_row, i]
					matrix[pivot_row, i] := matrix[step, i]
					matrix[step, i] := temp
					i := i + 1
				end
				 times_rows_swapped := times_rows_swapped + 1
			end

			pivot_element := matrix[step, step]

			Result := Result * pivot_element

			if not double_approx_equals (pivot_element, 0.0) then
				from
					i := step
				until
					i = matrix.width + 1
				loop
					matrix[step, i] := matrix[step, i] / pivot_element
					i := i + 1
				end
			else
				-- going out of the loop
				step := matrix.height
			end


			from
				row := step + 1
			until
				row = matrix.height + 1
			loop
				if not double_approx_equals (matrix[row, step], 0.0) then
					pivot_element := matrix [row, step]
					from
						i:= step
					until
						i = matrix.width + 1
					loop
						matrix[row, i] := (matrix[row, i] / pivot_element) - matrix[step, i]
						i := i + 1
					end
				end
				row := row + 1
			end

			step := step + 1
		end

		if times_rows_swapped.integer_remainder (2) = 1 then
			Result := -1 * Result
		end
	end

	double_approx_equals (a, b: DOUBLE): BOOLEAN
	local
		math: DOUBLE_MATH
	do
		create math
		Result := (math.dabs (a - b) < eps)
	end

	eps: DOUBLE = 0.000001
end

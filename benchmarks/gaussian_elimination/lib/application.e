note
	description : "Gaussian elimination benchmark. GPU implementation"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

feature
	clock_timer: CLOCK_TIME
	attribute
		create Result
	end

	make
	local
		ar_util: ARRAY_UTIL
		matrix_data: ARRAY2[REAL_64]
		matrix: G_MATRIX[REAL_64]
		determinant: REAL_64
		rows, columns: INTEGER
		start: INTEGER_64
	do
		rows := argument (1).to_integer_32
		columns := rows
		
		matrix_data := ar_util.create_random_double_matrix (rows, columns)
		
		start := clock_timer.clock
		create matrix.from_array2(matrix_data);
		
		determinant := gauss_determinant(matrix)

		print (clock_timer.formatted_diff(clock_timer.clock, start) + "%N")

		if determinant = 0.0 then
			print ("")
		end
	end
	
	gauss_determinant (matrix: G_MATRIX[DOUBLE]): DOUBLE
	require
		matrix.rows = matrix.columns
	local
		step, i: INTEGER
		pivot: DOUBLE
	do
		Result := 1
		from
			step := 0
		until
			step = matrix.rows
		loop
			pivot := matrix(step, step)
			Result := Result * pivot

			if not double_approx_equals (pivot, 0.0) then
				matrix.row (step).divided_by (pivot)
		 	else
		 		step := matrix.rows
			end
			from
				i := step + 1
			until
				i = matrix.rows
			loop
				pivot := matrix(i, step)
				if not double_approx_equals (pivot, 0.0) then
					matrix.row(i).divided_by (pivot)
					matrix.row(i).in_place_minus(matrix.row (step))
				end
				i := i + 1
			end

			step := step + 1
		end
	end

	double_approx_equals (a, b: DOUBLE): BOOLEAN
	local
		math: DOUBLE_MATH
	do
		create math
		Result := (math.dabs (a - b) < eps)
	end

	eps: DOUBLE = 0.000001
end

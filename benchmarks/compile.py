#!/usr/bin/env python
import os
import time
import subprocess
import collections
import statistics

# This file is a  part of set of scripts for benchmarking
# This file contains scripts for compiling Eiffel and CUDA projects.
# Eiffel projects can be compiled with or without assertion checking
# Benchmarks are structured as follows:
# benchmarks folder contains individual benchmarks
# Each of them has 3 sub-folders:
# 	lib - contains Eif2Cuda implementation.
#	C - contains CUDA implementation.
# 	eif - contains regular Eiffel implementation.
# for Eiffel projects, the project file always has name 'application.ecf'

# This script requires  MS VS compiler to be in PATH


# cleans and finalizes a single Eiffel project. 
# May keep or discard assertions, depending on 'keep_assertions' flag.
def compile_eiffel_project(path, keep_assertions):
	if (not os.path.exists(path)): # ignoring non-existent path.
		return
	oldDir = os.getcwd()
	print("Switching to " + path + "\n")
	os.chdir(path)
	
	compilation_list = ["ec.exe", "-clean" ,"-finalize"]
	
	if (keep_assertions): 
		compilation_list.append ("-keep")

	compilation_list.append("-c_compile")
	compilation_list.append("-config")
	compilation_list.append("application.ecf")

	print ("Compiling: " + " ".join(compilation_list) + "\n")
	
	from subprocess import call
	call(compilation_list)
	
	os.chdir(oldDir)

# compiles cuda project. Assumes it is contained in a single .cu file
def compile_cuda_project(name, path):
	if (not os.path.exists(path)): # ignoring non-existent path.
		return

	oldDir = os.getcwd()
	print("Switching to " + path + "\n")
	os.chdir(path)
	
	from subprocess import call
	call(["nvcc", "-m64", "-arch=compute_30", "-o", name, name + ".cu"])
	os.chdir(oldDir)
	
# Compiles all projects in the benchmark folder.
# uses keep_assertions flag to determine, 
# whether to keep runtime contract checking in Eiffel projects.
def compile_projects(path='.', keep_assertions=False):
	for dirname in os.listdir(path):
		compile_eiffel_project(os.path.join(dirname, "lib"), keep_assertions) # library implementation
		compile_eiffel_project(os.path.join(dirname, "eif"), keep_assertions) # regular Eiffel implementation
		compile_cuda_project(dirname, os.path.join(dirname, "c")) # Cuda implementation
note
	description: "Summary description for {QUICKSORT_EXAMPLE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class APPLICATION
inherit
	ARGUMENTS

create
	make
feature {NONE} --Implementation
	clock_timer: CLOCK_TIME
	attribute
		create Result
	end

	is_sorted (list: ARRAY [DOUBLE]): BOOLEAN
	local
		i: INTEGER
	do
		Result := True
		from
			i := list.lower + 1
		until
			i > list.upper
		loop
			Result := Result and list [i - 1] <= list [i]
			i := i + 1
		end
	end

	concatenate_array (a: ARRAY [DOUBLE]; b: ARRAY [DOUBLE]): ARRAY [DOUBLE]
	do
		create Result.make_from_array (a)
		across
			b as t
		loop
			Result.force (t.item, Result.upper + 1)
		end
	ensure
		same_size: a.count + b.count = Result.count
	end

	quicksort_array (list: ARRAY [DOUBLE]): ARRAY [DOUBLE]
	local
		less_a: ARRAY [DOUBLE]
		equal_a: ARRAY [DOUBLE]
		more_a: ARRAY [DOUBLE]
		pivot: DOUBLE
	do
		create less_a.make_empty
		create more_a.make_empty
		create equal_a.make_empty
		create Result.make_empty
		if list.count <= 1 then
			Result := list
		else
			pivot := list [list.lower]
			across
				list as li
			invariant
				less_a.count + equal_a.count + more_a.count <= list.count
			loop
				if li.item < pivot then
					less_a.force (li.item, less_a.upper + 1)
				elseif li.item = pivot then
					equal_a.force (li.item, equal_a.upper + 1)
				elseif li.item > pivot then
					more_a.force (li.item, more_a.upper + 1)
				end
			end
			Result := concatenate_array (Result, quicksort_array (less_a))
			Result := concatenate_array (Result, equal_a)
			Result := concatenate_array (Result, quicksort_array (more_a))
		end
	ensure
		same_size: list.count = Result.count
		sorted: is_sorted (Result)
	end


feature
	quicksort (a: ARRAY [DOUBLE]): ARRAY [DOUBLE]
	do
		Result := quicksort_array (a)
	end

	make
	local
		count: INTEGER
		data: ARRAY[DOUBLE]
		ar_util: ARRAY_UTIL
		profiler: ACTION_PROFILER
		sorted_cpu: ARRAY[DOUBLE]
		start: INTEGER_64
	do
		count := argument (1).to_integer_32

		data :=  ar_util.create_random_double_vector (count)

		start := clock_timer.clock
		sorted_cpu := quicksort (data)

		print (clock_timer.formatted_diff(clock_timer.clock, start) + "%N")

		if sorted_cpu.count = 0.0 then
			io.error.put_string ("")
		end
	end

	less_then(a, b: DOUBLE): BOOLEAN
	do
		Result := a < b
	end
end

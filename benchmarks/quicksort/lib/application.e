note
	description : "Quicksort benchmark. GPU implementation"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

feature
	clock_timer: CLOCK_TIME
	attribute
		create Result
	end

	make
	local
		ar_util: ARRAY_UTIL
		first_array: ARRAY[REAL_32]
		first, sorted: G_VECTOR[REAL_32]
		product: DOUBLE
		count: INTEGER
		start: INTEGER_64
	do
		count := argument (1).to_integer_32

		first_array := ar_util.create_random_float_vector (count)

		start := clock_timer.clock
		create first.from_array (first_array)

		sorted := quicksort(first)

		print (clock_timer.formatted_diff(clock_timer.clock, start) + "%N")

		if sorted.count = 0 then
			print ("")
		end
	end

	quicksort(a: G_VECTOR[REAL_32]): G_VECTOR[REAL_32]
	require
		a.count > 0
	local
		pivot: DOUBLE
		left, mid, right: G_VECTOR[REAL_32]
	do
		if (a.count = 1) then
			Result := a
		else
			pivot := a[a.count // 2]

			left := a.filter (agent (item: REAL_32; a_pivot: REAL_32): BOOLEAN do Result := item < a_pivot end)
			right := a.filter (agent (item: REAL_32; a_pivot: REAL_32): BOOLEAN do Result := item > a_pivot end)
			mid := a.filter (agent (item: REAL_32; a_pivot: REAL_32): BOOLEAN do Result := item = a_pivot end)

			Result := quicksort(left).concatenate(mid).concatenate(quicksort(right))
		end
	ensure
		Result.is_sorted
		Result.count = a.count
	end
end

#!/usr/bin/env python
import os
import time
import subprocess
import collections
import statistics
from compile import compile_projects

# A set of scripts for benchmarking
# Benchmarks are structured as follows:
# benchmarks folder contains individual benchmarks
# Each of them has 3 sub-folders:
# 	lib - contains Eif2Cuda implementation.
#	C - contains CUDA implementation.
# 	eif - contains regular Eiffel implementation.
# for Eiffel projects, the project file always has name 'application.ecf'

# This script requires  MS VS compiler to be in PATH

# cleans and finalizes a single Eiffel project. 
# May keep or discard assertions, depending on 'keep_assertions' flag.

# runs the specified project 'times' times with specified problem size. 
# returns the list of elapsed times.
def benchmark_project(name, path, log_file, size, times=10):
	if (not os.path.exists(path)): # ignoring non-existent path.
		return []
		
	oldDir = os.getcwd()
	print("Switching to " + path + "\n")
	os.chdir(path)
	
	print ("Benchmarking " + name + " " + str(size) + "\n")
	
	with open(log_file, "a+") as log:
		log.write(str(size) + ":\n")
	for i in range (times):	
		single_run([name + ".exe", str(size)], log_file)
		
	os.chdir(oldDir)

	
def single_run(list_to_execute, log_file):
	log = open(log_file, "a+")

	from subprocess import call
	call(list_to_execute, stderr=subprocess.DEVNULL, stdout=log)

	log.close()

def do_benchmark(path, subfolder, log_file, keep_assertions=False):

	for dirname in os.listdir(path):
		if os.path.isfile(dirname):
			continue
		with open(log_file, "a+") as log:
			log.write(dirname + ":\n")
		for size in [1000, 1000 * 10, 1000 * 100, 1000 * 1000, 1000 * 1000 * 10, 1000 * 1000 * 100]:

			benchmark_project(dirname, os.path.join(dirname, subfolder, "EIFGENs", dirname, "F_code"), log_file, size)

def do_c_benchmark(path, log_file):
	for dirname in os.listdir(path):
		if os.path.isfile(dirname):
			continue
		with open(log_file, "a+") as log:
			log.write(dirname + ":\n")
		for size in [1000, 1000 * 10, 1000 * 100, 1000 * 1000, 1000 * 1000 * 10, 1000 * 1000 * 100]:

			benchmark_project(dirname, os.path.join(dirname, "c"), log_file, size)

def benchmark_lib(path=".", keep_assertions=False):
	fd = open('lib_times.log', 'w')
	fd.close()
	do_benchmark(path, "lib", os.path.join(os.getcwd(), 'lib_times.log'),  keep_assertions)
		
def benchmark_eif(path=".", keep_assertions=False):
	fd = open("eif_times.log", "w")
	fd.close()
	do_benchmark(path, "eif", os.path.join(os.getcwd(),"eif_times.log"), keep_assertions)
	
def benchmark_c(path="."):
	fd = open("c_times.log", "w")
	fd.close()
	do_c_benchmark(path, os.path.join(os.getcwd(),"c_times.log"))


compile_projects()
#benchmark_lib()
#benchmark_eif()
benchmark_c()
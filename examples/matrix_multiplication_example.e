note
	description: "Summary description for {MATRIX_MULTIPLICATION_EXAMPLE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class MATRIX_MULTIPLICATION_EXAMPLE

feature
	run2
	local
		i : INTEGER
	do
		from
			i := 1
		until
			i = 101
		loop
			run
		end
	end

	run
	local
		rows, columns: INTEGER
		first, second: ARRAY2 [INTEGER]
		ar_util: ARRAY_UTIL
		kernel: CUDA_KERNEL [INTEGER]
		profiler: ACTION_PROFILER
		--cpu_result: ARRAY2 [INTEGER]
		cuda_result: ARRAY2 [INTEGER]
	do
		rows := 1000
		columns := rows

		first := profiler.report_function (agent ar_util.create_random_matrix (rows, columns), "generating first matrix").item
		second :=  profiler.report_function (agent ar_util.create_random_matrix (rows, columns), "generating second matrix").item
		--cpu_result := profiler.report_function (agent ar_util.multiply_matrices (first, second), "cpu matrix multiplication").item


		kernel := profiler.report_function (agent setup_kernel (first, second), "transferring data to device").item

		profiler.report_action (agent kernel.load_kernel ("CudaMatrixMultiplyKernel.ptx", "gpuMM"),
			"loading kernel")
		profiler.report_action (agent kernel.launch (grid_block_dim (rows, columns, thread_block_dim),
			thread_block_dim), "CUDA matrix multiplication")

		cuda_result := profiler.report_function (agent (kernel.output).to_array2, "copying data to host").item
		--kernel.dispose
		--print("Are matrices equal? " + cpu_result.is_equal (cuda_result).out + "%N")
	end

	thread_block_size: NATURAL_32 = 16

	thread_block_dim: DIM3
	do
		create Result.make_2d(thread_block_size, thread_block_size)
	end

	grid_block_dim (rows, columns: INTEGER; a_thread_block_dim: DIM3): DIM3
	local
		x, y: INTEGER
	do
		x := (rows.to_double / a_thread_block_dim.x).truncated_to_integer + 1
		y := (columns.to_double / a_thread_block_dim.y).truncated_to_integer + 1
		create Result.make_2d (x.to_natural_32, y.to_natural_32)
	end

	get_handles(first, second: ARRAY2[INTEGER]): ARRAY[CUDA_DATA_HANDLE[INTEGER]]
	local
		cuda: CUDA_ARRAY_INTEROP[INTEGER]
		h1, h2: CUDA_DATA_HANDLE[INTEGER]
		h3: CUDA_DATA_HANDLE[INTEGER]
	do
		create cuda
		cuda.initialize_cuda
		h1 := cuda.copy_multidimensional_array_to_gpu(first, dim_from_array_2d(first))
		h2 := cuda.copy_multidimensional_array_to_gpu(second, dim_from_array_2d(second))
		create h3.new_handle (h1.element_size,
			create {DATA_DIMENSIONS}.make_2d (h1.dimensions.dimension(0), h2.dimensions.dimension(1)))
		Result := (create {ARRAY_UTIL}).three_elements (h1, h2, h3)
	end

	dim_from_array_2d(array2d: ARRAY2[ANY]): DATA_DIMENSIONS
	do
		create Result.make_2d (array2d.height, array2d.width)
	end

	setup_kernel(first, second: ARRAY2[INTEGER]): CUDA_KERNEL[INTEGER]
	local
		input: like get_handles
	do
		input := get_handles (first, second)
		create Result.make (input, input[3])
	end
end

note
	description: "Example of transferring data from host to device and backwards."
	author: "Alexey Kolesnichenko."
	date: "18.06.2014"
	revision: "$Revision$"

class
	DATA_TRANSFER_EXAMPLE
feature
	run
	local
		original: ARRAY[INTEGER]
		from_device: ARRAY[INTEGER]
		array_util: ARRAY_UTIL
		cuda: CUDA_ARRAY_INTEROP[INTEGER]
	do
		original := array_util.create_random_vector(10)
		print("original array:%N")
		array_util.print_array (original)

		create cuda
		cuda.initialize_cuda
		print("Copy array to device and backwards.%N")
		from_device := cuda.copy_array_to_gpu (original).to_array
		print("A copy from device:%N")
		array_util.print_array (from_device)
	end
end

note
	description: "Summary description for {GENERATED_VECTOR_ADDITION_EXAMPLE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class GENERATED_VECTOR_ADDITION_EXAMPLE
feature
	run2
	local
		ar_util: ARRAY_UTIL
		first, second, sum: G_VECTOR[DOUBLE]
		count: INTEGER
	do
		count := 5000
		create first.from_array (ar_util.create_random_double_vector (count))
		create second.from_array (ar_util.create_random_double_vector (count))

		sum := first + second
	end

	run
	local
		ar_util: ARRAY_UTIL
		first, second, sum: G_VECTOR[INTEGER]
		count: INTEGER
	do
		count := 5
		create first.from_array (ar_util.create_random_vector (count))
		ar_util.print_array (first)

		create second.from_array (ar_util.create_random_vector (count))
		ar_util.print_array (second)

		sum := first + second - first
		ar_util.print_array (sum)

		sum := sum - first
		ar_util.print_array (sum)
		sum.multiplied_by (2)
		ar_util.print_array (sum)
	end
end

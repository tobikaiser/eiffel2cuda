note
	description: "Summary description for {G_VECTOR_TESTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class G_VECTOR_TESTS
inherit
	EQA_XUNIT_TEST_SET
feature
	test_subvectors
	local
		vec: G_VECTOR[INTEGER]
		subvec: G_VECTOR[INTEGER]
	do
		create vec.from_array (<<1, 2, 42, 5>>)
		subvec := vec.subvector_of_length (1, 3)
		assert_equals (3, subvec.count, "wrong size of subvector")
		assert_equals (2, subvec.to_array[1], "wrong 1st element of subvector")
		assert_equals (42, subvec.to_array[2], "wrong 2nd element of subvector")
		assert_equals (5, subvec.to_array[3], "wrong 3rd element of subvector")
	end

	test_indexation
	local
		vec: G_VECTOR[INTEGER]
	do
		create vec.from_array (<<3, 6, -15, -77>>)
		assert_equals (3, vec[0], "wrong 0 element of subvector")
		assert_equals (6, vec[1], "wrong 1st element of subvector")
		assert_equals (-15, vec[2], "wrong 2nd element of subvector")
		assert_equals (-77, vec[3], "wrong 3rd element of subvector")
	end

	test_min
	local
		vec: G_VECTOR[INTEGER]
	do
		create vec.from_array (<<3, 6, -15, -77>>)
		assert_equals (-77, vec.min, "wrong min element")
	end

	test_max
	local
		vec: G_VECTOR[INTEGER]
	do
		create vec.from_array (<<3, 6, -15, -77>>)
		assert_equals (6, vec.max, "wrong max element")
	end
end

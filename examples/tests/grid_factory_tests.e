note
	description: "Summary description for {GRID_FACTORY_TESTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class GRID_FACTORY_TESTS
inherit
	EQA_XUNIT_TEST_SET
feature
	test_grid_coverage
	local
		gf: GRID_FACTORY
	do
		create gf
		
		assert_equals ({NATURAL} 1, gf.default_grid_dim_1d (1).x, "grid is of wrong size.")
		assert_equals ({NATURAL} 1, gf.default_grid_dim_1d (gf.default_block_size - 1).x, "grid is of wrong size.")
		assert_equals ({NATURAL} 1, gf.default_grid_dim_1d (gf.default_block_size).x, "grid is of wrong size.")
		assert_equals ({NATURAL} 2, gf.default_grid_dim_1d (gf.default_block_size + 1).x, "grid is of wrong size.")
		assert_equals ({NATURAL} 2, gf.default_grid_dim_1d (2 * gf.default_block_size).x, "grid is of wrong size.")
		assert_equals ({NATURAL} 3, gf.default_grid_dim_1d (2 * gf.default_block_size + 1).x, "grid is of wrong size.")
	end
end

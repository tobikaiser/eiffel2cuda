note
	description: "Summary description for {DATA_DIMENSIONS_TESTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	DATA_DIMENSIONS_TESTS
inherit
	EQA_XUNIT_TEST_SET

feature
	test_1d_data
	local
		data_dim: DATA_DIMENSIONS
	do
		create data_dim.make_1d (11)

		assert_equals (1, data_dim.dimensions, "wrong dimensions size")
		assert_equals (11, data_dim.dimension (0), "wrong dimension")
		assert_equals (11, data_dim.total_dimension_size, "wrong total dimensions.")
	end

	test_2d_data
	local
		data_dim: DATA_DIMENSIONS
	do
		create data_dim.make_2d (11, 7)

		assert_equals (2, data_dim.dimensions, "wrong dimensions size")
		assert_equals (11, data_dim.dimension (0), "wrong dimension")
		assert_equals (7, data_dim.dimension (1), "wrong dimension")
		assert_equals (11 * 7, data_dim.total_dimension_size, "wrong total dimensions.")
	end

	test_3d_data
	local
		list: ARRAYED_LIST[INTEGER]
		data_dim: DATA_DIMENSIONS
	do
		create list.make (3)
		list.extend (10)
		list.extend (3)
		list.extend (5)

		create data_dim.make (list.to_array)
		assert_equals (3, data_dim.dimensions, "wrong dimensions size")
		assert_equals (10, data_dim.dimension (0), "wrong dimension")
		assert_equals (3, data_dim.dimension (1), "wrong dimension")
		assert_equals (5, data_dim.dimension (2), "wrong dimension")
		assert_equals (150, data_dim.total_dimension_size, "wrong total dimensions.")
	end
end

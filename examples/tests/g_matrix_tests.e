note
	description: "Summary description for {G_MATRIX_TESTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class G_MATRIX_TESTS
inherit
	EQA_XUNIT_TEST_SET
feature
	test_row_access
	local
		matrix: G_MATRIX[DOUBLE]
	do
		create matrix.from_folded_array (
			<<
				1.5, -2.0,
				3.1, 4.2
			>>,
			2, 2)

		assert_equals (1.5,  matrix.row (0)[0], "wrong 0 element in 0 row")
		assert_equals (-2.0,  matrix.row (0)[1], "wrong 1 element in 0 row")
		assert_equals (3.1,  matrix.row (1)[0], "wrong 0 element in 1 row")
		assert_equals (4.2,  matrix.row (1)[1], "wrong 1 element in 1 row")
	end

	test_multiply_by_element
	local
		matrix: G_MATRIX[DOUBLE]
	do
		create matrix.from_folded_array (
			<<
				1.5, -2.0,
				3.1, 4.2
			>>,
			2, 2)

		matrix.multiplied_by (2.0)
		assert_equals ( 3.0,  matrix.row (0)[0], "wrong 0 element in 0 row")
		assert_equals (-4.0,  matrix.row (0)[1], "wrong 1 element in 0 row")
		assert_equals (6.2,  matrix.row (1)[0], "wrong 0 element in 1 row")
		assert_equals (8.4,  matrix.row (1)[1], "wrong 1 element in 1 row")
		end
end

note
	description: "Summary description for {VECTOR_TEMPLATE_TESTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_TEMPLATE_TESTS
inherit
	EQA_XUNIT_TEST_SET
feature
	test_parameter_name_generation
	local
		template: VECTOR_TEMPLATE
		ar_util: ARRAY_UTIL
		params: ARRAY[KERNEL_PARAMETER]
		lower_index : INTEGER
	do
		params := ar_util.three_elements (create {KERNEL_PARAMETER}.make ("float" , 1),
			create {KERNEL_PARAMETER}.make ("float", 2),
			create {KERNEL_PARAMETER}.make ("double" ,1))

		create template.make ("add", "+", params)
		lower_index := 1
		assert_equals (3, template.kernel_prototype.count, "wrong number of parameters")

		assert_equals ("vec_a", template.kernel_prototype[lower_index].name, "wrong name of 1st parameter")
		assert_equals ("vec_b", template.kernel_prototype[lower_index + 1].name, "wrong name of 2nd parameter")
		assert_equals ("vec_c", template.kernel_prototype[lower_index + 2].name, "wrong name of 3rd parameter")
	end

	test_kernel_prototype
	local
		template: VECTOR_TEMPLATE
		ar_util: ARRAY_UTIL
		params: ARRAY[KERNEL_PARAMETER]
		s: STRING
	do
		params := ar_util.three_elements (create {KERNEL_PARAMETER}.make ("float" , 1),
			create {KERNEL_PARAMETER}.make ("float", 2),
			create {KERNEL_PARAMETER}.make ("double" ,1))

		create template.make ("add", "+", params)
		assert_equals (
			"extern %"C%"%N__global__ void add(float* vec_a, int n_vec_a, float* vec_b, int n_vec_b, double* vec_c, int n_vec_c) {%N",
			template.generate_prototype, "wrong kernel prototype")
	end
end

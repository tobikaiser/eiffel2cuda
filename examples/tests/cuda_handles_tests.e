note
	description: "Summary description for {CUDA_HANDLES_TESTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class CUDA_HANDLES_TESTS
inherit
	EQA_XUNIT_TEST_SET

feature
	test_fresh_handle
	local
		ch: CUDA_DATA_HANDLE[INTEGER]
	do
		create ch.new_handle_default(create {DATA_DIMENSIONS}.make_1d (10))
		assert_equals (10, ch.to_array.count, "wrong element count!")
	end

	test_concatenate
	local
		cuda: CUDA_ARRAY_INTEROP[INTEGER]
		h1, h2, h3: CUDA_DATA_HANDLE[INTEGER]
	do
		create cuda
		h1 := cuda.copy_single_item_to_gpu (-5)
		h2 := cuda.copy_single_item_to_gpu (42)
		h3 := cuda.concatenate (h1, h2)

		assert_equals(1, h3.dimensions.dimensions, "wrong dimensions")
		assert_equals (2, h3.x_dimension, "wrong number of elements")
		assert_equals (-5, h3.at (0), "wrong first element")
		assert_equals (42, h3.at (1), "wrong second element")
	end

	test_empty_concatenate
	local
		cuda: CUDA_ARRAY_INTEROP[INTEGER]
		h1, empty, h3: CUDA_DATA_HANDLE[INTEGER]
	do
		create cuda
		h1 := cuda.copy_single_item_to_gpu (-5)
		create empty.empty
		h3 := cuda.concatenate (h1, empty)

		assert_equals(1, h3.dimensions.dimensions, "wrong dimensions")
		assert_equals (1, h3.x_dimension, "wrong number of elements")
		assert_equals (-5, h3.at (0), "wrong first element")

		h3 := cuda.concatenate (empty, h1)

		assert_equals(1, h3.dimensions.dimensions, "wrong dimensions")
		assert_equals (1, h3.x_dimension, "wrong number of elements")
		assert_equals (-5, h3.at (0), "wrong first element")
	end


	test_handle_to_array2
	local
		cuda: CUDA_ARRAY_INTEROP[DOUBLE]
		ch: CUDA_DATA_HANDLE[DOUBLE]
		ar2: ARRAY2[DOUBLE]
		ar_util: ARRAY_UTIL
		ar_list: ARRAYED_LIST[DOUBLE]
	do
		create cuda
		create ar_list.make (4)
		ar_list.extend (42.0)
		ar_list.extend (0.0)
		ar_list.extend (0.0)
		ar_list.extend (42.0)
		ch := cuda.copy_multidimensional_array_to_gpu (ar_list.to_array, --ar_util.four_elements(42.0, 0.0, 0.0, 42.0),
			create {DATA_DIMENSIONS}.make_2d (2, 2))
		ar2 := ch.to_array2

		assert_equals (2, ar2.height, "wrong number of rows in the matrix")
		assert_equals (2, ar2.width, "wrong number of columnss in the matrix")
		assert_equals (42.0, ar2[ar2.lower, ar2.lower], "wrong element at the position [1,1]")
		assert_equals (0.0, ar2[ar2.lower, ar2.lower + 1], "wrong element at the position [1,2]")
		assert_equals (0.0, ar2[ar2.lower + 1, ar2.lower], "wrong element at the position [2,1]")
		assert_equals (42.0, ar2[ar2.lower + 1, ar2.lower + 1], "wrong element at the position [2,2]")
	end
end

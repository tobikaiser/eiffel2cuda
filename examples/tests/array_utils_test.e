note
	description: "Summary description for {ARRAY_UTILS_TEST}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ARRAY_UTILS_TEST
inherit
	EQA_XUNIT_TEST_SET

feature
	one_element_test
	local
		util: ARRAY_UTIL
		ar: ARRAY[STRING]
	do
		ar := util.one_element ("hello")

		assert_equals(1, ar.count, "wrong array size")
		assert_equals("hello",  ar[ar.lower], "wrong array content")
	end

	two_element_test
	local
		util: ARRAY_UTIL
		ar: ARRAY[STRING]
	do
		ar := util.two_elements ("hello", "world")

		assert_equals(2, ar.count, "wrong array size")
		assert_equals("hello",  ar[ar.lower], "wrong array content")
		assert_equals("world",  ar[ar.lower + 1], "wrong array content")
	end

	random_array_test
	local
		count: INTEGER
		util: ARRAY_UTIL
		ar: ARRAY[INTEGER]
	do
		count := 11
		ar := util.create_random_vector (count)

		assert_equals (count, ar.count, "wrong array size")
	end
end

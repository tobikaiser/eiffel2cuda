note
	description: "Summary description for {OBJECT_SIZES_TESTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	OBJECT_SIZES_TESTS
inherit
	EQA_XUNIT_TEST_SET
feature
	int32_test
	local
		sizer: CUSTOM_SIZE[INTEGER]
	do
		assert("wrong size", sizer.size_of (0) = 4)
	end

	char_test
	local
		sizer: CUSTOM_SIZE[CHARACTER_8]
	do
		assert("wrong size", sizer.size_of ('0') = 1)
	end

	double_test
	local
		sizer: CUSTOM_SIZE[DOUBLE]
	do
		assert("wrong size", sizer.size_of (1.0) = 8)
	end

	any_test
	local
		sizer: CUSTOM_SIZE[ANY]
	do
		assert("wrong size", sizer.size_of (create {ANY}) = 8)
	end

	size_by_type_test
	do
		assert_equals ({NATURAL} 4, (create {CUSTOM_SIZE[INTEGER]}).size_of_default , "wrong size")
		assert_equals ({NATURAL} 1, (create {CUSTOM_SIZE[CHARACTER_8]}).size_of_default, "wrong size")
		assert_equals ({NATURAL} 8, (create {CUSTOM_SIZE[DOUBLE]}).size_of_default, "wrong size")
		assert_equals ({NATURAL} 8, (create {CUSTOM_SIZE[ANY]}).size_of_default, "wrong size")
	end
end

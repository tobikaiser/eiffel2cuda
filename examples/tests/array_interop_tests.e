note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	ARRAY_INTEROP_TESTS

inherit
	EQA_XUNIT_TEST_SET

feature -- Test routines

	cuda_array_handle_test
	local
		array_interop: CUDA_ARRAY_INTEROP[INTEGER]
		array_util : ARRAY_UTIL
		cuda_h: CUDA_DATA_HANDLE[INTEGER]
		number_of_elements: INTEGER
	do
		create array_interop
		array_interop.initialize_cuda
		number_of_elements := 100
		cuda_h := array_interop.copy_array_to_gpu (array_util.create_random_vector (number_of_elements))
		assert("memory is not copied to device.", not cuda_h.pointer.is_default_pointer)
		assert("wrong number of elements in the handle.", cuda_h.dimensions.dimension (0) = number_of_elements)
		assert("wrong element size.", cuda_h.element_size = 4)

	end

	cuda_array2_handle_test
	local
		int_array: ARRAY2[INTEGER]
		array_interop: CUDA_ARRAY_INTEROP[INTEGER]
		array_util : ARRAY_UTIL
		cuda_h: CUDA_DATA_HANDLE[INTEGER]
	do
		create array_interop
		array_interop.initialize_cuda
		int_array := array_util.create_random_matrix (10, 9)
		cuda_h := array_interop.copy_multidimensional_array_to_gpu (int_array,
			create {DATA_DIMENSIONS}.make_2d (int_array.height, int_array.width))
		assert("memory is not copied to device.", not cuda_h.pointer.is_default_pointer)
		assert("wrong number of elements in the handle.", cuda_h.dimensions.dimension (0) = 10)
		assert("wrong number of elements in the handle.", cuda_h.dimensions.dimension (1) = 9)
		assert("wrong element size.", cuda_h.element_size = 4)
		assert("", int_array.area.to_array.is_equal (cuda_h.to_array))
	end

	cuda_is_available_test
			-- tests that cuda is available at target system.
	local
		cuda : CUDA_ARRAY_INTEROP[ANY]
	do
		create cuda
		assert("no cuda devices", cuda.is_cuda_available)
	end
end



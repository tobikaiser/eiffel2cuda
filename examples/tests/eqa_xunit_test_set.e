note
	description: "Summary description for {EQA_EXTENDED_ASSERT_TEST}."
	author: "Provides access to X-unit style"
	date: "$Date$"
	revision: "$Revision$"

deferred class EQA_XUNIT_TEST_SET
inherit
	EQA_TEST_SET
feature {NONE}
	assert_equals(expected: ANY; actual: like expected; err_msg: STRING)
	do
		if not actual.is_equal(expected) then
			assert (err_msg + "%Nexpected " + expected.out + ", got " + actual.out, false)
		end
	end

	assert_is_null(actual: ANY; err_msg: STRING)
	do
		if not (actual = VOID) then
			assert (err_msg + "%Nexpected " + actual.out + "to be void." + actual.out, false)
		end
	end
end

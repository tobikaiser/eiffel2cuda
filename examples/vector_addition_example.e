note
	description: "Summary description for {VECTOR_ADDITION_EXAMPLE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	VECTOR_ADDITION_EXAMPLE

feature {NONE} -- data
	cuda_result: ARRAY[INTEGER]
	attribute
		create Result.make_empty
	end

	cpu_result: ARRAY[INTEGER]
	attribute
		create Result.make_empty
	end
feature
	run
	local
		first: ARRAY[INTEGER]
		second: ARRAY[INTEGER]
		array_util: ARRAY_UTIL
		cuda: CUDA_ARRAY_INTEROP[INTEGER]
		kernel: CUDA_KERNEL[INTEGER]
		input_size: INTEGER
		profiler: ACTION_PROFILER
	do
		input_size := 1600000
		print ("array size:" + input_size.out + "%N")

		-- creating data
		first := profiler.report_function (agent array_util.create_random_vector(input_size), "generating first array").item
		second := profiler.report_function (agent array_util.create_random_vector(input_size), "generating second array").item

		-- cpu operation
		profiler.report_action (agent add_vectors (first, second), "CPU vector addition")

		create cuda
		cuda.initialize_cuda

		-- loading and launching kernel
		kernel := profiler.report_function(agent setup_kernel (first, second), "copying data to device").item
		profiler.report_action (agent kernel.load_kernel ("CudaVectorAddKernel.ptx", "add"), "loading kernel")

		profiler.report_action (agent kernel.launch( create {DIM3}.make_1d(((input_size.as_natural_32 / 1024).truncated_to_integer + 1).max(1)),
			create {DIM3}.make_1d (1024)), "CUDA vector addition")
		cuda_result := profiler.report_function (agent (kernel.output).to_array,
			"copying data to host").item

		kernel.dispose
		-- verifying cuda result
		print("Are arrays equal? " + cuda_result.is_equal (cpu_result).out + "%N")
	end

	add_vectors(first, second: ARRAY[INTEGER])
	local
		i: INTEGER
	do
		create cpu_result.make_filled (0, 1, first.count)
		from
			i := 1
		until
			i > first.count
		loop
			cpu_result[i] := first[i] + second[i]
			i := i + 1
		end
	end

	get_handles(first, second: ARRAY[INTEGER]): ARRAY[CUDA_DATA_HANDLE[INTEGER]]
	local
		cuda: CUDA_ARRAY_INTEROP[INTEGER]
		h1, h2: CUDA_DATA_HANDLE[INTEGER]
		h3: CUDA_DATA_HANDLE[INTEGER]
	do
		create cuda
		h1 := cuda.copy_array_to_gpu (first)
		h2 := cuda.copy_array_to_gpu (second)
		create h3.new_handle (h1.element_size, h1.dimensions)
		Result := (create {ARRAY_UTIL}).three_elements (h1, h2, h3)
	end

	setup_kernel(first, second: ARRAY[INTEGER]): CUDA_KERNEL[INTEGER]
	local
		input: like get_handles
	do
		input := get_handles (first, second)
		create Result.make (input, input[3])
	end
end

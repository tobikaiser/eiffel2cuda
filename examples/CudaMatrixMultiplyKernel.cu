#include <math.h>
#include <assert.h>
#include <stdio.h>


template <typename T> __device__ T& elem(T* data, int width, int i, int j) {
	return data[i * width + j];
}

extern "C" __global__ void gpuMM(
	int* A, int a_rows, int a_cols, 
	int* B, int b_rows, int b_cols, 
	int* C, int c_rows, int c_cols)
{
	//if (threadIdx.x == 0 && threadIdx.y == 0) {
	//	printf("kernel setup: grid: (%d x %d), block: ( %d x %d)\n", gridDim.x, gridDim.y, blockDim.x, blockDim.y);
	//}

	// Matrix multiplication for NxN matrices C=A*B
	// Each thread computes a single element of C
	int row = blockIdx.y*blockDim.y + threadIdx.y;
	int col = blockIdx.x*blockDim.x + threadIdx.x;

	if (row < a_rows && col < b_cols) {
		int sum = 0;
		for (int k = 0; k < a_cols; ++k) {
			sum += elem(A, a_rows, row, k) * elem(B, b_rows, k, col); // A[row*a_cols+k]*B[k*a_cols+col];
		}
		elem(C, c_rows, row, col) = sum; //C[row*a_rows+col] = sum;
		__syncthreads();
	}
}

note
	description : "eiffel2gpu_samples application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
	local
		cuda_h: CUDA_HELPER
	do
		print("Device compute capabilities:" + cuda_h.compute_capability.out + "%N")
		print("Total Device Memory: " + cuda_h.total_device_memory.megabytes.out + " Mb.%N")
		cuda_h.debug_print_available_memory


		run_min_example
		--run_gaussian_elimination
		--run_arithmetic

		print("%N")
	end

	run_data_transfer
	do
		print("%N")
		print("%N")
		print("data transfer example%N")
		(create {DATA_TRANSFER_EXAMPLE}).run
	end
	run_indexation
	do
		print("%N")
		print("%N")
		print("gVector indexation example%N")
		(create {G_VECTOR_INDEXATION}).run
	end

	run_arithmetic
	do
		print("%N")
		print("%N")
		print("gVector arithmetics example%N")
		(create {GENERATED_VECTOR_ADDITION_EXAMPLE}).run
	end

	run_gaussian_elimination
	do
		print("%N")
		print("%N")
		print("gaussian elimination example%N")
		(create {GAUSSIAN_ELIMINATION_EXAMPLE}).run
	end

	run_matrix_multiplication
	do
		print("%N")
		print("%N")
		print("matrix multiplication example%N")
		(create {MATRIX_MULTIPLICATION_EXAMPLE}).run
	end

	run_min_example
	do
		print("%N")
		print("%N")
		print("minimum in array example%N")
		(create {FIND_MIN_EXAMPLE}).run
	end

end

note
	description: "Summary description for {G_VECTOR_INDEXATION}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	G_VECTOR_INDEXATION
feature
run
	local
		first: ARRAY[INTEGER]
		g_first: G_VECTOR[INTEGER]
		input_size: INTEGER
		cpu_r, gpu_r: INTEGER_64
		profiler: ACTION_PROFILER
	do
		input_size := 1000000
		print ("array size:" + input_size.out + "%N")

		-- creating data
		first := profiler.report_function (agent create_random_vector (input_size), "generating data array").item
		create g_first.from_array (first)
		-- cpu operation
		 profiler.report_action (agent sum (first), "CPU vector sum")
		-- cpu operation
		 profiler.report_action (agent g_sum (g_first), "GPU vector sum")

		cpu_r := sum (first)
		gpu_r := g_sum (g_first)
		print("cpu sum: " + cpu_r.out + "%N")
		print("gpu sum: " + gpu_r.out + "%N")
		-- verifying cuda result
		print("Are sums equal? " + cpu_r.is_equal (gpu_r).out + "%N")
	end

	sum(first: ARRAY[INTEGER]): INTEGER_64
	local
		i: INTEGER
	do
		Result := 0
		from
			i := 1
		until
			i > first.count
		loop
			Result := Result + first[i]
			i := i + 1
		end
	end

	g_sum(first: G_VECTOR[INTEGER]): INTEGER_64
	local
		i: INTEGER
	do
		Result := 0
		from
			i := 0
		until
			i = first.count
		loop
			Result := Result + first[i]
			i := i + 1
		end
	end

	create_random_vector (size: INTEGER): ARRAY[INTEGER]
	require
		size >= 1
	local
		i: INTEGER
		rand: C_RAND
	do
		create Result.make_filled (0, 1, size)
		create rand.default_create
		from
			i := 1
		until
			i = size + 1
		loop
			Result[i] := to_limit(rand.rand, 100)
			i := i + 1
		end
	end

	to_limit (num: INTEGER; limit: INTEGER) : INTEGER
	do
		Result := num \\ limit
	end
end

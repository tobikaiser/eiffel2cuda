note
	description: "Summary description for {INNER_PRODUCT}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class INNER_PRODUCT
inherit
	STD_FILES
feature
	dot_product: DOUBLE
	g_dot_product: like dot_product
	profiler: ACTION_PROFILER

	run
	local
		ar_util: ARRAY_UTIL
		first, second: ARRAY[like dot_product]
		g_first, g_second: G_VECTOR[like dot_product]
		count: INTEGER
	do
		count := 1000 * 1000 * 140
		print ("enter array size:%N")

		input.read_integer_32
		count := input.last_integer

		first := profiler.report_function (agent ar_util.create_random_double_vector (count),
			"Generating random array of " + count.out +" elements.").item
		second := profiler.report_function (agent ar_util.create_random_double_vector (count),
			"Generating random array of " + count.out +" elements.").item

		create g_first.from_array (first)
		create g_second.from_array (second)
		profiler.report_action (agent inner_product (first, second), "computing CPU inner product...")
		profiler.report_action (agent g_inner_product (g_first, g_second) , "computing GPU inner product...")

		print("CPU min: " + dot_product.out + "%N")
		print("GPU min: " + g_dot_product.out + "%N")
	end

	g_inner_product(a, b: G_VECTOR[DOUBLE]): DOUBLE
	require
		a.count = b.count
	do

		Result := a.compwise_multiplication (b).sum
		g_dot_product := Result
	end
	
	inner_product(a, b: ARRAY[DOUBLE]): DOUBLE
	require
		a.count = b.count
	local
		i: INTEGER
	do
		from
			i := 1
		until
			i = a.count + 1
		loop
			Result := Result + a[i] * b[i]
			i := i + 1
		end

		dot_product := Result
	end
end

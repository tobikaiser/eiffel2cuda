note
	description: "Summary description for {GAUSSIAN_ELIMINATION_EXAMPLE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class GAUSSIAN_ELIMINATION_EXAMPLE
feature
	cpu_det: DOUBLE
	gpu_det: DOUBLE
	profiler: ACTION_PROFILER
	run
	local
		matrix: ARRAY2[DOUBLE]
		g_matrix: G_MATRIX[DOUBLE]
		ar_util: ARRAY_UTIL
	do
		--create g_matrix.from_folded_array (
		--	<<	2,1,
		--		1,4>>,
		--	2, 2)
		matrix := profiler.report_function (agent ar_util.create_random_double_matrix (100, 100), "creating matrix 1000 x 1000%N").item
		create g_matrix.from_array2(matrix)

		matrix := g_matrix.to_array2
		--ar_util.print_matrix (matrix)
		profiler.report_action (agent compute_det (matrix), "computing CPU determinant")

		print("%NCPU determinant:" +  cpu_det.out + "%N")
		profiler.report_action (agent g_compute_det (g_matrix), "computing GPU determinant")
		print("%NGPU determinant:" +  gpu_det.out + "%N")
	end

	compute_det (matrix: ARRAY2[DOUBLE])
	do
		cpu_det := det(matrix)
	end

	g_compute_det (matrix: G_MATRIX[DOUBLE])
	do
		gpu_det := g_det (matrix)
	end

	det (matrix: ARRAY2[DOUBLE]): DOUBLE
	require
		is_square: matrix.height = matrix.width
	local
		step, pivot_row, i, row, times_rows_swapped: INTEGER
		math: DOUBLE_MATH
		temp, pivot_element: DOUBLE
	do
		Result := 1.0
		create math
		from
			step := 1
		until
			step = matrix.height + 1
		loop
			pivot_row := step
			from
				i:= step + 1
			until
				i = matrix.height + 1
			loop
				if math.dabs (matrix[i, step]) > math.dabs (matrix[pivot_row, step]) then
					pivot_row := i
				end

				i := i + 1
			end

			if pivot_row /= step then
				-- swap rows
				from
					i := 1
				until
					i = matrix.width + 1
				loop
					temp := matrix[pivot_row, i]
					matrix[pivot_row, i] := matrix[step, i]
					matrix[step, i] := temp
					i := i + 1
				end
				 times_rows_swapped := times_rows_swapped + 1
			end

			pivot_element := matrix[step, step]

			Result := Result * pivot_element

			if not double_approx_equals (pivot_element, 0.0) then
				from
					i := step
				until
					i = matrix.width + 1
				loop
					matrix[step, i] := matrix[step, i] / pivot_element
					i := i + 1
				end
			else
				-- going out of the loop
				step := matrix.height
			end


			from
				row := step + 1
			until
				row = matrix.height + 1
			loop
				if not double_approx_equals (matrix[row, step], 0.0) then
					pivot_element := matrix [row, step]
					from
						i:= step
					until
						i = matrix.width + 1
					loop
						matrix[row, i] := (matrix[row, i] / pivot_element) - matrix[step, i]
						i := i + 1
					end
				end
				row := row + 1
			end

			step := step + 1
		end

		if times_rows_swapped.integer_remainder (2) = 1 then
			Result := -1 * Result
		end
	end

	g_det (matrix: G_MATRIX[DOUBLE]): DOUBLE
	local
		step, i: INTEGER
		pivot: DOUBLE
	do
		Result := 1
		from
			step := 0
		until
			step = matrix.rows
		loop
			pivot := matrix.row(step)[step]
			Result := Result * pivot

			if not double_approx_equals (pivot, 0.0) then
				matrix.row (step).divided_by (pivot)
		 	else
		 		step := matrix.rows
			end
			from
				i := step + 1
			until
				i = matrix.rows
			loop
				pivot := matrix.row (i)[step]
				if not double_approx_equals (pivot, 0.0) then
					matrix.row(i).divided_by (pivot)
					matrix.row(i).in_place_minus(matrix.row (step))
				end
				i := i + 1
			end

			step := step + 1
		end
	end



	double_approx_equals (a, b: DOUBLE): BOOLEAN
	local
		math: DOUBLE_MATH
	do
		create math
		Result := (math.dabs (a - b) < eps)
	end

	eps: DOUBLE = 0.000001
end

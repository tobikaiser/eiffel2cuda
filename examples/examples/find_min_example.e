note
	description: "Summary description for {FIND_MIN_EXAMPLE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class FIND_MIN_EXAMPLE
inherit
	STD_FILES
feature
	min: DOUBLE
	g_min: DOUBLE
	profiler: ACTION_PROFILER
	run
	local
		ar_util: ARRAY_UTIL
		vector: ARRAY[DOUBLE]
		g_vector: G_VECTOR[DOUBLE]
		count: INTEGER
	do
		count := 1000 * 1000 * 140
		print ("enter array size:%N")

		input.read_integer_32
		count := input.last_integer

		vector := profiler.report_function (agent ar_util.create_random_double_vector (count),
			"Generating random array of " + count.out +" elements.").item

		create g_vector.from_array (vector)
		profiler.report_action (agent find_min (vector), "computing CPU min...")
		profiler.report_action (agent g_find_min (g_vector) , "computing GPU min...")

		print("CPU min: " + min.out + "%N")
		print("GPU min: " + g_min.out + "%N")
	end

	find_min (vector: ARRAY[DOUBLE])
	local
		i: INTEGER
		temp: DOUBLE
	do
		temp := vector[vector.lower]

		from
			i := vector.lower + 1
		until
			i = vector.upper + 1
		loop
			if temp > vector[i] then
				temp := vector[i]
			end
			i := i + 1
		end

		min := temp
	end

	g_find_min (g_vector: G_VECTOR[DOUBLE])
	do
		g_min := g_vector.min
	end
end

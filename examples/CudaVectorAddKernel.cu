extern "C"
__global__ void add(int* a, int n_a, int* b, int n_b, int* sum, int n_sum)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;

	if (i < n_a) {
		sum[i] = a[i] + b[i];
	}
}